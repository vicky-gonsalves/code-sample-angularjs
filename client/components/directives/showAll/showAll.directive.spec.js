'use strict';

describe('Directive: showAll', function () {

  // load the directive's module
  beforeEach(module('windmanagerApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<show-all></show-all>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the showAll directive');
  }));
});
