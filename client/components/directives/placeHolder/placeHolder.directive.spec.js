'use strict';

describe('Directive: placeHolder', function () {

  // load the directive's module
  beforeEach(module('windmanagerApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<place-holder></place-holder>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the placeHolder directive');
  }));
});
