'use strict';

describe('Controller: ListBladeCtrl', function () {

  // load the controller's module
  beforeEach(module('windmanagerApp'));

  var ListBladeCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ListBladeCtrl = $controller('ListBladeCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
