'use strict';

describe('Directive: siteMap', function () {

  // load the directive's module and view
  beforeEach(module('windmanagerApp'));
  beforeEach(module('components/directives/site/siteMap/siteMap.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<site-map></site-map>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the siteMap directive');
  }));
});
