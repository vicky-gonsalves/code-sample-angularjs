'use strict';

var app = require('../..');
import request from 'supertest';

var newFinding;

describe('Finding API:', function() {

  describe('GET /api/findings', function() {
    var findings;

    beforeEach(function(done) {
      request(app)
        .get('/api/findings')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          findings = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      findings.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/findings', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/findings')
        .send({
          name: 'New Finding',
          info: 'This is the brand new finding!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newFinding = res.body;
          done();
        });
    });

    it('should respond with the newly created finding', function() {
      newFinding.name.should.equal('New Finding');
      newFinding.info.should.equal('This is the brand new finding!!!');
    });

  });

  describe('GET /api/findings/:id', function() {
    var finding;

    beforeEach(function(done) {
      request(app)
        .get('/api/findings/' + newFinding._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          finding = res.body;
          done();
        });
    });

    afterEach(function() {
      finding = {};
    });

    it('should respond with the requested finding', function() {
      finding.name.should.equal('New Finding');
      finding.info.should.equal('This is the brand new finding!!!');
    });

  });

  describe('PUT /api/findings/:id', function() {
    var updatedFinding;

    beforeEach(function(done) {
      request(app)
        .put('/api/findings/' + newFinding._id)
        .send({
          name: 'Updated Finding',
          info: 'This is the updated finding!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedFinding = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedFinding = {};
    });

    it('should respond with the updated finding', function() {
      updatedFinding.name.should.equal('Updated Finding');
      updatedFinding.info.should.equal('This is the updated finding!!!');
    });

  });

  describe('DELETE /api/findings/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/findings/' + newFinding._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when finding does not exist', function(done) {
      request(app)
        .delete('/api/findings/' + newFinding._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
