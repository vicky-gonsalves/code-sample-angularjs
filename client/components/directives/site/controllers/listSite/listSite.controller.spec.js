'use strict';

describe('Controller: ListSiteCtrl', function () {

  // load the controller's module
  beforeEach(module('windmanagerApp'));

  var ListSiteCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ListSiteCtrl = $controller('ListSiteCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
