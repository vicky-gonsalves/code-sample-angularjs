// // page init
// jQuery(function(){
// 	initTabs();
// 	initCustomForms();
// 	initOpenClose();
// 	initMobileNav();
// 	initCustomHover();
// 	jQuery('input, textarea').placeholder();
// });

// initialize custom form elements
function initCustomForms() {
  jcf.replaceAll();
}

// content tabs init
function initTabs() {
  jQuery('ul.tabset').tabset({
    tabLinks: 'a',
    defaultTab: false
  });
}

// open-close init
function initOpenClose() {
  // jQuery('.projectbox').openClose({
  //   activeClass: 'projects-active',
  //   opener: '.opener',
  //   slider: '.holder',
  //   animSpeed: 400,
  //   effect: 'slide'
  // });
  // jQuery('.projectbox').openClose({
  //   activeClass: 'items-active',
  //   opener: '.show-all',
  //   slider: '.slide',
  //   animSpeed: 400,
  //   effect: 'slide'
  // });
  /*TODO for next pages*/
  jQuery('.blade-details').openClose({
    activeClass: 'active',
    opener: '.open',
    slider: '.detail-slide',
    animSpeed: 400,
    effect: 'slide'
  });
  jQuery('.more-info').openClose({
    activeClass: 'active2',
    opener: '.open2',
    slider: '.slide2',
    animSpeed: 400,
    effect: 'slide'
  });
}

// mobile menu init
function initMobileNav() {
  jQuery('body').mobileNav({
    hideOnClickOutside: true,
    menuActiveClass: 'header-active',
    menuOpener: '.header-opener',
    menuDrop: '#header'
  });
  jQuery('body').mobileNav({
    hideOnClickOutside: true,
    menuActiveClass: 'menu-active',
    menuOpener: '.menu-opener',
    menuDrop: '.right-info'
  });
  jQuery('body').mobileNav({
    hideOnClickOutside: true,
    menuActiveClass: 'listing-active',
    menuOpener: '.btn-listing',
    menuDrop: '.listing-block'
  });
}

// add classes on hover/touch
function initCustomHover() {
  jQuery('.info-tooltip').touchHover();
}

/*
 * jQuery Tabs plugin
 */

;(function($, $win) {
  'use strict';

  function Tabset($holder, options) {
    this.$holder = $holder;
    this.options = options;

    this.init();
  }

  Tabset.prototype = {
    init: function() {
      this.$tabLinks = this.$holder.find(this.options.tabLinks);

      this.setStartActiveIndex();
      this.setActiveTab();

      if (this.options.autoHeight) {
        this.$tabHolder = $(this.$tabLinks.eq(0).attr(this.options.attrib)).parent();
      }
    },

    setStartActiveIndex: function() {
      var $classTargets = this.getClassTarget(this.$tabLinks);
      var $activeLink = $classTargets.filter('.' + this.options.activeClass);
      var $hashLink = this.$tabLinks.filter('[' + this.options.attrib + '="' + location.hash + '"]');
      var activeIndex;

      if (this.options.checkHash && $hashLink.length) {
        $activeLink = $hashLink;
      }

      activeIndex = $classTargets.index($activeLink);

      this.activeTabIndex = this.prevTabIndex = (activeIndex === -1 ? (this.options.defaultTab ? 0 : null) : activeIndex);
    },

    setActiveTab: function() {
      var self = this;

      this.$tabLinks.each(function(i, link) {
        var $link = $(link);
        var $classTarget = self.getClassTarget($link);
        var $tab = $($link.attr(self.options.attrib));

        if (i !== self.activeTabIndex) {
          $classTarget.removeClass(self.options.activeClass);
          $tab.addClass(self.options.tabHiddenClass).removeClass(self.options.activeClass);
        } else {
          $classTarget.addClass(self.options.activeClass);
          $tab.removeClass(self.options.tabHiddenClass).addClass(self.options.activeClass);
        }

        self.attachTabLink($link, i);
      });
    },

    attachTabLink: function($link, i) {
      var self = this;

      $link.on(this.options.event + '.tabset', function(e) {
        e.preventDefault();

        if (self.activeTabIndex === self.prevTabIndex && self.activeTabIndex !== i) {
          self.activeTabIndex = i;
          self.switchTabs();
        }
      });
    },

    resizeHolder: function(height) {
      var self = this;

      if (height) {
        this.$tabHolder.height(height);
        setTimeout(function() {
          self.$tabHolder.addClass('transition');
        }, 10);
      } else {
        self.$tabHolder.removeClass('transition').height('');
      }
    },

    switchTabs: function() {
      var self = this;

      var $prevLink = this.$tabLinks.eq(this.prevTabIndex);
      var $nextLink = this.$tabLinks.eq(this.activeTabIndex);

      var $prevTab = this.getTab($prevLink);
      var $nextTab = this.getTab($nextLink);

      $prevTab.removeClass(this.options.activeClass);

      if (self.haveTabHolder()) {
        this.resizeHolder($prevTab.outerHeight());
      }

      setTimeout(function() {
        self.getClassTarget($prevLink).removeClass(self.options.activeClass);

        $prevTab.addClass(self.options.tabHiddenClass);
        $nextTab.removeClass(self.options.tabHiddenClass).addClass(self.options.activeClass);

        self.getClassTarget($nextLink).addClass(self.options.activeClass);

        if (self.haveTabHolder()) {
          self.resizeHolder($nextTab.outerHeight());

          setTimeout(function() {
            self.resizeHolder();
            self.prevTabIndex = self.activeTabIndex;
          }, self.options.animSpeed);
        } else {
          self.prevTabIndex = self.activeTabIndex;
        }
      }, this.options.autoHeight ? this.options.animSpeed : 1);
    },

    getClassTarget: function($link) {
      return this.options.addToParent ? $link.parent() : $link;
    },

    getActiveTab: function() {
      return this.getTab(this.$tabLinks.eq(this.activeTabIndex));
    },

    getTab: function($link) {
      return $($link.attr(this.options.attrib));
    },

    haveTabHolder: function() {
      return this.$tabHolder && this.$tabHolder.length;
    },

    destroy: function() {
      var self = this;

      this.$tabLinks.off('.tabset').each(function() {
        var $link = $(this);

        self.getClassTarget($link).removeClass(self.options.activeClass);
        $($link.attr(self.options.attrib)).removeClass(self.options.activeClass + ' ' + self.options.tabHiddenClass);
      });

      this.$holder.removeData('Tabset');
    }
  };

  $.fn.tabset = function(options) {
    options = $.extend({
      activeClass: 'active',
      addToParent: false,
      autoHeight: false,
      checkHash: false,
      defaultTab: true,
      animSpeed: 500,
      tabLinks: 'a',
      attrib: 'href',
      event: 'click',
      tabHiddenClass: 'js-tab-hidden'
    }, options);
    options.autoHeight = options.autoHeight && $.support.opacity;

    return this.each(function() {
      var $holder = $(this);

      if (!$holder.data('Tabset')) {
        $holder.data('Tabset', new Tabset($holder, options));
      }
    });
  };
}(jQuery, jQuery(window)));

/*
 * jQuery Open/Close plugin
 */
;(function($) {
  function  OpenClose(options) {
    this.options = $.extend({
      addClassBeforeAnimation: true,
      hideOnClickOutside: false,
      activeClass: 'active',
      opener: '.opener',
      slider: '.slide',
      animSpeed: 400,
      effect: 'fade',
      event: 'click'
    }, options);
    this.init();
  }

  OpenClose.prototype = {
    init: function() {
      if (this.options.holder) {
        this.findElements();
        this.attachEvents();
        this.makeCallback('onInit', this);
      }
    },
    findElements: function() {
      this.holder = $(this.options.holder);
      this.opener = this.holder.find(this.options.opener);
      this.slider = this.holder.find(this.options.slider);
    },
    attachEvents: function() {
      // add handler
      var self = this;
      this.eventHandler = function(e) {
        e.preventDefault();
        if (self.slider.hasClass(slideHiddenClass)) {
          self.showSlide();
        } else {
          self.hideSlide();
        }
      };
      self.opener.bind(self.options.event, this.eventHandler);

      // hover mode handler
      if (self.options.event === 'over') {
        self.opener.bind('mouseenter', function() {
          if (!self.holder.hasClass(self.options.activeClass)) {
            self.showSlide();
          }
        });
        self.holder.bind('mouseleave', function() {
          self.hideSlide();
        });
      }

      // outside click handler
      self.outsideClickHandler = function(e) {
        if (self.options.hideOnClickOutside) {
          var target = $(e.target);
          if (!target.is(self.holder) && !target.closest(self.holder).length) {
            self.hideSlide();
          }
        }
      };

      // set initial styles
      if (this.holder.hasClass(this.options.activeClass)) {
        $(document).bind('click touchstart', self.outsideClickHandler);
      } else {
        this.slider.addClass(slideHiddenClass);
      }
    },
    showSlide: function() {
      var self = this;
      if (self.options.addClassBeforeAnimation) {
        self.holder.addClass(self.options.activeClass);
      }
      self.slider.removeClass(slideHiddenClass);
      $(document).bind('click touchstart', self.outsideClickHandler);

      self.makeCallback('animStart', true);
      toggleEffects[self.options.effect].show({
        box: self.slider,
        speed: self.options.animSpeed,
        complete: function() {
          if (!self.options.addClassBeforeAnimation) {
            self.holder.addClass(self.options.activeClass);
          }
          self.makeCallback('animEnd', true);
        }
      });
    },
    hideSlide: function() {
      var self = this;
      if (self.options.addClassBeforeAnimation) {
        self.holder.removeClass(self.options.activeClass);
      }
      $(document).unbind('click touchstart', self.outsideClickHandler);

      self.makeCallback('animStart', false);
      toggleEffects[self.options.effect].hide({
        box: self.slider,
        speed: self.options.animSpeed,
        complete: function() {
          if (!self.options.addClassBeforeAnimation) {
            self.holder.removeClass(self.options.activeClass);
          }
          self.slider.addClass(slideHiddenClass);
          self.makeCallback('animEnd', false);
        }
      });
    },
    destroy: function() {
      this.slider.removeClass(slideHiddenClass).css({display: ''});
      this.opener.unbind(this.options.event, this.eventHandler);
      this.holder.removeClass(this.options.activeClass).removeData('OpenClose');
      $(document).unbind('click touchstart', this.outsideClickHandler);
    },
    makeCallback: function(name) {
      if (typeof this.options[name] === 'function') {
        var args = Array.prototype.slice.call(arguments);
        args.shift();
        this.options[name].apply(this, args);
      }
    }
  };

  // add stylesheet for slide on DOMReady
  var slideHiddenClass = 'js-slide-hidden';
  (function() {
    var tabStyleSheet = $('<style type="text/css">')[0];
    var tabStyleRule = '.' + slideHiddenClass;
    tabStyleRule += '{position:absolute !important;left:-9999px !important;top:-9999px !important;display:block !important}';
    if (tabStyleSheet.styleSheet) {
      tabStyleSheet.styleSheet.cssText = tabStyleRule;
    } else {
      tabStyleSheet.appendChild(document.createTextNode(tabStyleRule));
    }
    $('head').append(tabStyleSheet);
  }());

  // animation effects
  var toggleEffects = {
    slide: {
      show: function(o) {
        o.box.stop(true).hide().slideDown(o.speed, o.complete);
      },
      hide: function(o) {
        o.box.stop(true).slideUp(o.speed, o.complete);
      }
    },
    fade: {
      show: function(o) {
        o.box.stop(true).hide().fadeIn(o.speed, o.complete);
      },
      hide: function(o) {
        o.box.stop(true).fadeOut(o.speed, o.complete);
      }
    },
    none: {
      show: function(o) {
        o.box.hide().show(0, o.complete);
      },
      hide: function(o) {
        o.box.hide(0, o.complete);
      }
    }
  };

  // jQuery plugin interface
  $.fn.openClose = function(opt) {
    return this.each(function() {
      jQuery(this).data('OpenClose', new OpenClose($.extend(opt, {holder: this})));
    });
  };
}(jQuery));

/*
 * Simple Mobile Navigation
 */
;(function($) {
  function MobileNav(options) {
    this.options = $.extend({
      container: null,
      hideOnClickOutside: false,
      menuActiveClass: 'nav-active',
      menuOpener: '.nav-opener',
      menuDrop: '.nav-drop',
      toggleEvent: 'click',
      outsideClickEvent: 'click touchstart pointerdown MSPointerDown'
    }, options);
    this.initStructure();
    this.attachEvents();
  }

  MobileNav.prototype = {
    initStructure: function() {
      this.page = $('html');
      this.container = $(this.options.container);
      this.opener = this.container.find(this.options.menuOpener);
      this.drop = this.container.find(this.options.menuDrop);
    },
    attachEvents: function() {
      var self = this;

      if (activateResizeHandler) {
        activateResizeHandler();
        activateResizeHandler = null;
      }

      this.outsideClickHandler = function(e) {
        if (self.isOpened()) {
          var target = $(e.target);
          if (!target.closest(self.opener).length && !target.closest(self.drop).length) {
            self.hide();
          }
        }
      };

      this.openerClickHandler = function(e) {
        e.preventDefault();
        self.toggle();
      };

      this.opener.on(this.options.toggleEvent, this.openerClickHandler);
    },
    isOpened: function() {
      return this.container.hasClass(this.options.menuActiveClass);
    },
    show: function() {
      this.container.addClass(this.options.menuActiveClass);
      if (this.options.hideOnClickOutside) {
        this.page.on(this.options.outsideClickEvent, this.outsideClickHandler);
      }
    },
    hide: function() {
      this.container.removeClass(this.options.menuActiveClass);
      if (this.options.hideOnClickOutside) {
        this.page.off(this.options.outsideClickEvent, this.outsideClickHandler);
      }
    },
    toggle: function() {
      if (this.isOpened()) {
        this.hide();
      } else {
        this.show();
      }
    },
    destroy: function() {
      this.container.removeClass(this.options.menuActiveClass);
      this.opener.off(this.options.toggleEvent, this.clickHandler);
      this.page.off(this.options.outsideClickEvent, this.outsideClickHandler);
    }
  };

  var activateResizeHandler = function() {
    var win = $(window),
      doc = $('html'),
      resizeClass = 'resize-active',
      flag, timer;
    var removeClassHandler = function() {
      flag = false;
      doc.removeClass(resizeClass);
    };
    var resizeHandler = function() {
      if (!flag) {
        flag = true;
        doc.addClass(resizeClass);
      }
      clearTimeout(timer);
      timer = setTimeout(removeClassHandler, 500);
    };
    win.on('resize orientationchange', resizeHandler);
  };

  $.fn.mobileNav = function(options) {
    return this.each(function() {
      var params = $.extend({}, options, {container: this}),
        instance = new MobileNav(params);
      $.data(this, 'MobileNav', instance);
    });
  };
}(jQuery));

/*
 * Mobile hover plugin
 */
;(function($) {

  // detect device type
  var isTouchDevice = ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch,
    isWinPhoneDevice = /Windows Phone/.test(navigator.userAgent);

  // define events
  var eventOn = (isTouchDevice && 'touchstart') || (isWinPhoneDevice && navigator.pointerEnabled && 'pointerdown') || (isWinPhoneDevice && navigator.msPointerEnabled && 'MSPointerDown') || 'mouseenter',
    eventOff = (isTouchDevice && 'touchend') || (isWinPhoneDevice && navigator.pointerEnabled && 'pointerup') || (isWinPhoneDevice && navigator.msPointerEnabled && 'MSPointerUp') || 'mouseleave';

  // event handlers
  var toggleOn, toggleOff, preventHandler;
  if (isTouchDevice || isWinPhoneDevice) {
    // prevent click handler
    preventHandler = function(e) {
      e.preventDefault();
    };

    // touch device handlers
    toggleOn = function(e) {
      var options = e.data, element = $(this);

      var toggleOff = function(e) {
        var target = $(e.target);
        if (!target.is(element) && !target.closest(element).length) {
          element.removeClass(options.hoverClass);
          element.off('click', preventHandler);
          if (options.onLeave) options.onLeave(element);
          $(document).off(eventOn, toggleOff);
        }
      };

      if (!element.hasClass(options.hoverClass)) {
        element.addClass(options.hoverClass);
        element.one('click', preventHandler);
        $(document).on(eventOn, toggleOff);
        if (options.onHover) options.onHover(element);
      }
    };
  } else {
    // desktop browser handlers
    toggleOn = function(e) {
      var options = e.data, element = $(this);
      element.addClass(options.hoverClass);
      $(options.context).on(eventOff, options.selector, options, toggleOff);
      if (options.onHover) options.onHover(element);
    };
    toggleOff = function(e) {
      var options = e.data, element = $(this);
      element.removeClass(options.hoverClass);
      $(options.context).off(eventOff, options.selector, toggleOff);
      if (options.onLeave) options.onLeave(element);
    };
  }

  // jQuery plugin
  $.fn.touchHover = function(opt) {
    var options = $.extend({
      context: document,
      selector: '.info-tooltip',
      hoverClass: 'hover'
    }, opt);
    $(options.context).on(eventOn, options.selector, options, toggleOn);
    return this;
  };
}(jQuery));

/*! http://mths.be/placeholder v2.0.7 by @mathias */
;(function(window, document, $) {

  // Opera Mini v7 doesn’t support placeholder although its DOM seems to indicate so
  var isOperaMini = Object.prototype.toString.call(window.operamini) == '[object OperaMini]';
  var isInputSupported = 'placeholder' in document.createElement('input') && !isOperaMini;
  var isTextareaSupported = 'placeholder' in document.createElement('textarea') && !isOperaMini;
  var prototype = $.fn;
  var valHooks = $.valHooks;
  var propHooks = $.propHooks;
  var hooks;
  var placeholder;

  if (isInputSupported && isTextareaSupported) {

    placeholder = prototype.placeholder = function() {
      return this;
    };

    placeholder.input = placeholder.textarea = true;

  } else {

    placeholder = prototype.placeholder = function() {
      var $this = this;
      $this
        .filter((isInputSupported ? 'textarea' : ':input') + '[placeholder]')
        .not('.placeholder')
        .bind({
          'focus.placeholder': clearPlaceholder,
          'blur.placeholder': setPlaceholder
        })
        .data('placeholder-enabled', true)
        .trigger('blur.placeholder');
      return $this;
    };

    placeholder.input = isInputSupported;
    placeholder.textarea = isTextareaSupported;

    hooks = {
      'get': function(element) {
        var $element = $(element);

        var $passwordInput = $element.data('placeholder-password');
        if ($passwordInput) {
          return $passwordInput[0].value;
        }

        return $element.data('placeholder-enabled') && $element.hasClass('placeholder') ? '' : element.value;
      },
      'set': function(element, value) {
        var $element = $(element);

        var $passwordInput = $element.data('placeholder-password');
        if ($passwordInput) {
          return $passwordInput[0].value = value;
        }

        if (!$element.data('placeholder-enabled')) {
          return element.value = value;
        }
        if (value == '') {
          element.value = value;
          // Issue #56: Setting the placeholder causes problems if the element continues to have focus.
          if (element != safeActiveElement()) {
            // We can't use `triggerHandler` here because of dummy text/password inputs :(
            setPlaceholder.call(element);
          }
        } else if ($element.hasClass('placeholder')) {
          clearPlaceholder.call(element, true, value) || (element.value = value);
        } else {
          element.value = value;
        }
        // `set` can not return `undefined`; see http://jsapi.info/jquery/1.7.1/val#L2363
        return $element;
      }
    };

    if (!isInputSupported) {
      valHooks.input = hooks;
      propHooks.value = hooks;
    }
    if (!isTextareaSupported) {
      valHooks.textarea = hooks;
      propHooks.value = hooks;
    }

    $(function() {
      // Look for forms
      $(document).delegate('form', 'submit.placeholder', function() {
        // Clear the placeholder values so they don't get submitted
        var $inputs = $('.placeholder', this).each(clearPlaceholder);
        setTimeout(function() {
          $inputs.each(setPlaceholder);
        }, 10);
      });
    });

    // Clear placeholder values upon page reload
    $(window).bind('beforeunload.placeholder', function() {
      $('.placeholder').each(function() {
        this.value = '';
      });
    });

  }

  function args(elem) {
    // Return an object of element attributes
    var newAttrs = {};
    var rinlinejQuery = /^jQuery\d+$/;
    $.each(elem.attributes, function(i, attr) {
      if (attr.specified && !rinlinejQuery.test(attr.name)) {
        newAttrs[attr.name] = attr.value;
      }
    });
    return newAttrs;
  }

  function clearPlaceholder(event, value) {
    var input = this;
    var $input = $(input);
    if (input.value == $input.attr('placeholder') && $input.hasClass('placeholder')) {
      if ($input.data('placeholder-password')) {
        $input = $input.hide().next().show().attr('id', $input.removeAttr('id').data('placeholder-id'));
        // If `clearPlaceholder` was called from `$.valHooks.input.set`
        if (event === true) {
          return $input[0].value = value;
        }
        $input.focus();
      } else {
        input.value = '';
        $input.removeClass('placeholder');
        input == safeActiveElement() && input.select();
      }
    }
  }

  function setPlaceholder() {
    var $replacement;
    var input = this;
    var $input = $(input);
    var id = this.id;
    if (input.value == '') {
      if (input.type == 'password') {
        if (!$input.data('placeholder-textinput')) {
          try {
            $replacement = $input.clone().attr({'type': 'text'});
          } catch (e) {
            $replacement = $('<input>').attr($.extend(args(this), {'type': 'text'}));
          }
          $replacement
            .removeAttr('name')
            .data({
              'placeholder-password': $input,
              'placeholder-id': id
            })
            .bind('focus.placeholder', clearPlaceholder);
          $input
            .data({
              'placeholder-textinput': $replacement,
              'placeholder-id': id
            })
            .before($replacement);
        }
        $input = $input.removeAttr('id').hide().prev().attr('id', id).show();
        // Note: `$input[0] != input` now!
      }
      $input.addClass('placeholder');
      $input[0].value = $input.attr('placeholder');
    } else {
      $input.removeClass('placeholder');
    }
  }

  function safeActiveElement() {
    // Avoid IE9 `document.activeElement` of death
    // https://github.com/mathiasbynens/jquery-placeholder/pull/99
    try {
      return document.activeElement;
    } catch (err) {
    }
  }

}(this, document, jQuery));

/*!
 * JavaScript Custom Forms
 *
 * Copyright 2014-2015 PSD2HTML - http://psd2html.com/jcf
 * Released under the MIT license (LICENSE.txt)
 *
 * Version: 1.1.3
 */
;(function(root, factory) {
  'use strict';
  if (typeof define === 'function' && define.amd) {
    define(['jquery'], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(require('jquery'));
  } else {
    root.jcf = factory(jQuery);
  }
}(this, function($) {
  'use strict';

  // define version
  var version = '1.1.3';

  // private variables
  var customInstances = [];

  // default global options
  var commonOptions = {
    optionsKey: 'jcf',
    dataKey: 'jcf-instance',
    rtlClass: 'jcf-rtl',
    focusClass: 'jcf-focus',
    pressedClass: 'jcf-pressed',
    disabledClass: 'jcf-disabled',
    hiddenClass: 'jcf-hidden',
    resetAppearanceClass: 'jcf-reset-appearance',
    unselectableClass: 'jcf-unselectable'
  };

  // detect device type
  var isTouchDevice = ('ontouchstart' in window) || window.DocumentTouch && document instanceof window.DocumentTouch,
    isWinPhoneDevice = /Windows Phone/.test(navigator.userAgent);
  commonOptions.isMobileDevice = !!(isTouchDevice || isWinPhoneDevice);

  // create global stylesheet if custom forms are used
  var createStyleSheet = function() {
    var styleTag = $('<style>').appendTo('head'),
      styleSheet = styleTag.prop('sheet') || styleTag.prop('styleSheet');

    // crossbrowser style handling
    var addCSSRule = function(selector, rules, index) {
      if (styleSheet.insertRule) {
        styleSheet.insertRule(selector + '{' + rules + '}', index);
      } else {
        styleSheet.addRule(selector, rules, index);
      }
    };

    // add special rules
    addCSSRule('.' + commonOptions.hiddenClass, 'position:absolute !important;left:-9999px !important;height:1px !important;width:1px !important;margin:0 !important;border-width:0 !important;-webkit-appearance:none;-moz-appearance:none;appearance:none');
    addCSSRule('.' + commonOptions.rtlClass + ' .' + commonOptions.hiddenClass, 'right:-9999px !important; left: auto !important');
    addCSSRule('.' + commonOptions.unselectableClass, '-webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; -webkit-tap-highlight-color: rgba(0,0,0,0);');
    addCSSRule('.' + commonOptions.resetAppearanceClass, 'background: none; border: none; -webkit-appearance: none; appearance: none; opacity: 0; filter: alpha(opacity=0);');

    // detect rtl pages
    var html = $('html'), body = $('body');
    if (html.css('direction') === 'rtl' || body.css('direction') === 'rtl') {
      html.addClass(commonOptions.rtlClass);
    }

    // handle form reset event
    html.on('reset', function() {
      setTimeout(function() {
        api.refreshAll();
      }, 0);
    });

    // mark stylesheet as created
    commonOptions.styleSheetCreated = true;
  };

  // simplified pointer events handler
  (function() {
    var pointerEventsSupported = navigator.pointerEnabled || navigator.msPointerEnabled,
      touchEventsSupported = ('ontouchstart' in window) || window.DocumentTouch && document instanceof window.DocumentTouch,
      eventList, eventMap = {}, eventPrefix = 'jcf-';

    // detect events to attach
    if (pointerEventsSupported) {
      eventList = {
        pointerover: navigator.pointerEnabled ? 'pointerover' : 'MSPointerOver',
        pointerdown: navigator.pointerEnabled ? 'pointerdown' : 'MSPointerDown',
        pointermove: navigator.pointerEnabled ? 'pointermove' : 'MSPointerMove',
        pointerup: navigator.pointerEnabled ? 'pointerup' : 'MSPointerUp'
      };
    } else {
      eventList = {
        pointerover: 'mouseover',
        pointerdown: 'mousedown' + (touchEventsSupported ? ' touchstart' : ''),
        pointermove: 'mousemove' + (touchEventsSupported ? ' touchmove' : ''),
        pointerup: 'mouseup' + (touchEventsSupported ? ' touchend' : '')
      };
    }

    // create event map
    $.each(eventList, function(targetEventName, fakeEventList) {
      $.each(fakeEventList.split(' '), function(index, fakeEventName) {
        eventMap[fakeEventName] = targetEventName;
      });
    });

    // jQuery event hooks
    $.each(eventList, function(eventName, eventHandlers) {
      eventHandlers = eventHandlers.split(' ');
      $.event.special[eventPrefix + eventName] = {
        setup: function() {
          var self = this;
          $.each(eventHandlers, function(index, fallbackEvent) {
            if (self.addEventListener) self.addEventListener(fallbackEvent, fixEvent, false);
            else self['on' + fallbackEvent] = fixEvent;
          });
        },
        teardown: function() {
          var self = this;
          $.each(eventHandlers, function(index, fallbackEvent) {
            if (self.addEventListener) self.removeEventListener(fallbackEvent, fixEvent, false);
            else self['on' + fallbackEvent] = null;
          });
        }
      };
    });

    // check that mouse event are not simulated by mobile browsers
    var lastTouch = null;
    var mouseEventSimulated = function(e) {
      var dx = Math.abs(e.pageX - lastTouch.x),
        dy = Math.abs(e.pageY - lastTouch.y),
        rangeDistance = 25;

      if (dx <= rangeDistance && dy <= rangeDistance) {
        return true;
      }
    };

    // normalize event
    var fixEvent = function(e) {
      var origEvent = e || window.event,
        touchEventData = null,
        targetEventName = eventMap[origEvent.type];

      e = $.event.fix(origEvent);
      e.type = eventPrefix + targetEventName;

      if (origEvent.pointerType) {
        switch (origEvent.pointerType) {
          case 2:
            e.pointerType = 'touch';
            break;
          case 3:
            e.pointerType = 'pen';
            break;
          case 4:
            e.pointerType = 'mouse';
            break;
          default:
            e.pointerType = origEvent.pointerType;
        }
      } else {
        e.pointerType = origEvent.type.substr(0, 5); // "mouse" or "touch" word length
      }

      if (!e.pageX && !e.pageY) {
        touchEventData = origEvent.changedTouches ? origEvent.changedTouches[0] : origEvent;
        e.pageX = touchEventData.pageX;
        e.pageY = touchEventData.pageY;
      }

      if (origEvent.type === 'touchend') {
        lastTouch = {x: e.pageX, y: e.pageY};
      }
      if (e.pointerType === 'mouse' && lastTouch && mouseEventSimulated(e)) {
        return;
      } else {
        return ($.event.dispatch || $.event.handle).call(this, e);
      }
    };
  }());

  // custom mousewheel/trackpad handler
  (function() {
    var wheelEvents = ('onwheel' in document || document.documentMode >= 9 ? 'wheel' : 'mousewheel DOMMouseScroll').split(' '),
      shimEventName = 'jcf-mousewheel';

    $.event.special[shimEventName] = {
      setup: function() {
        var self = this;
        $.each(wheelEvents, function(index, fallbackEvent) {
          if (self.addEventListener) self.addEventListener(fallbackEvent, fixEvent, false);
          else self['on' + fallbackEvent] = fixEvent;
        });
      },
      teardown: function() {
        var self = this;
        $.each(wheelEvents, function(index, fallbackEvent) {
          if (self.addEventListener) self.removeEventListener(fallbackEvent, fixEvent, false);
          else self['on' + fallbackEvent] = null;
        });
      }
    };

    var fixEvent = function(e) {
      var origEvent = e || window.event;
      e = $.event.fix(origEvent);
      e.type = shimEventName;

      // old wheel events handler
      if ('detail' in origEvent) {
        e.deltaY = -origEvent.detail;
      }
      if ('wheelDelta' in origEvent) {
        e.deltaY = -origEvent.wheelDelta;
      }
      if ('wheelDeltaY' in origEvent) {
        e.deltaY = -origEvent.wheelDeltaY;
      }
      if ('wheelDeltaX' in origEvent) {
        e.deltaX = -origEvent.wheelDeltaX;
      }

      // modern wheel event handler
      if ('deltaY' in origEvent) {
        e.deltaY = origEvent.deltaY;
      }
      if ('deltaX' in origEvent) {
        e.deltaX = origEvent.deltaX;
      }

      // handle deltaMode for mouse wheel
      e.delta = e.deltaY || e.deltaX;
      if (origEvent.deltaMode === 1) {
        var lineHeight = 16;
        e.delta *= lineHeight;
        e.deltaY *= lineHeight;
        e.deltaX *= lineHeight;
      }

      return ($.event.dispatch || $.event.handle).call(this, e);
    };
  }());

  // extra module methods
  var moduleMixin = {
    // provide function for firing native events
    fireNativeEvent: function(elements, eventName) {
      $(elements).each(function() {
        var element = this, eventObject;
        if (element.dispatchEvent) {
          eventObject = document.createEvent('HTMLEvents');
          eventObject.initEvent(eventName, true, true);
          element.dispatchEvent(eventObject);
        } else if (document.createEventObject) {
          eventObject = document.createEventObject();
          eventObject.target = element;
          element.fireEvent('on' + eventName, eventObject);
        }
      });
    },
    // bind event handlers for module instance (functions beggining with "on")
    bindHandlers: function() {
      var self = this;
      $.each(self, function(propName, propValue) {
        if (propName.indexOf('on') === 0 && $.isFunction(propValue)) {
          // dont use $.proxy here because it doesn't create unique handler
          self[propName] = function() {
            return propValue.apply(self, arguments);
          };
        }
      });
    }
  };

  // public API
  var api = {
    version: version,
    modules: {},
    getOptions: function() {
      return $.extend({}, commonOptions);
    },
    setOptions: function(moduleName, moduleOptions) {
      if (arguments.length > 1) {
        // set module options
        if (this.modules[moduleName]) {
          $.extend(this.modules[moduleName].prototype.options, moduleOptions);
        }
      } else {
        // set common options
        $.extend(commonOptions, moduleName);
      }
    },
    addModule: function(proto) {
      // add module to list
      var Module = function(options) {
        // save instance to collection
        if (!options.element.data(commonOptions.dataKey)) {
          options.element.data(commonOptions.dataKey, this);
        }
        customInstances.push(this);

        // save options
        this.options = $.extend({}, commonOptions, this.options, getInlineOptions(options.element), options);

        // bind event handlers to instance
        this.bindHandlers();

        // call constructor
        this.init.apply(this, arguments);
      };

      // parse options from HTML attribute
      var getInlineOptions = function(element) {
        var dataOptions = element.data(commonOptions.optionsKey),
          attrOptions = element.attr(commonOptions.optionsKey);

        if (dataOptions) {
          return dataOptions;
        } else if (attrOptions) {
          try {
            return $.parseJSON(attrOptions);
          } catch (e) {
            // ignore invalid attributes
          }
        }
      };

      // set proto as prototype for new module
      Module.prototype = proto;

      // add mixin methods to module proto
      $.extend(proto, moduleMixin);
      if (proto.plugins) {
        $.each(proto.plugins, function(pluginName, plugin) {
          $.extend(plugin.prototype, moduleMixin);
        });
      }

      // override destroy method
      var originalDestroy = Module.prototype.destroy;
      Module.prototype.destroy = function() {
        this.options.element.removeData(this.options.dataKey);

        for (var i = customInstances.length - 1; i >= 0; i--) {
          if (customInstances[i] === this) {
            customInstances.splice(i, 1);
            break;
          }
        }

        if (originalDestroy) {
          originalDestroy.apply(this, arguments);
        }
      };

      // save module to list
      this.modules[proto.name] = Module;
    },
    getInstance: function(element) {
      return $(element).data(commonOptions.dataKey);
    },
    replace: function(elements, moduleName, customOptions) {
      var self = this,
        instance;

      if (!commonOptions.styleSheetCreated) {
        createStyleSheet();
      }

      $(elements).each(function() {
        var moduleOptions,
          element = $(this);

        instance = element.data(commonOptions.dataKey);
        if (instance) {
          instance.refresh();
        } else {
          if (!moduleName) {
            $.each(self.modules, function(currentModuleName, module) {
              if (module.prototype.matchElement.call(module.prototype, element)) {
                moduleName = currentModuleName;
                return false;
              }
            });
          }
          if (moduleName) {
            moduleOptions = $.extend({element: element}, customOptions);
            instance = new self.modules[moduleName](moduleOptions);
          }
        }
      });
      return instance;
    },
    refresh: function(elements) {
      $(elements).each(function() {
        var instance = $(this).data(commonOptions.dataKey);
        if (instance) {
          instance.refresh();
        }
      });
    },
    destroy: function(elements) {
      $(elements).each(function() {
        var instance = $(this).data(commonOptions.dataKey);
        if (instance) {
          instance.destroy();
        }
      });
    },
    replaceAll: function(context) {
      var self = this;
      $.each(this.modules, function(moduleName, module) {
        $(module.prototype.selector, context).each(function() {
          if (this.className.indexOf('jcf-ignore') < 0) {
            self.replace(this, moduleName);
          }
        });
      });
    },
    refreshAll: function(context) {
      if (context) {
        $.each(this.modules, function(moduleName, module) {
          $(module.prototype.selector, context).each(function() {
            var instance = $(this).data(commonOptions.dataKey);
            if (instance) {
              instance.refresh();
            }
          });
        });
      } else {
        for (var i = customInstances.length - 1; i >= 0; i--) {
          customInstances[i].refresh();
        }
      }
    },
    destroyAll: function(context) {
      if (context) {
        $.each(this.modules, function(moduleName, module) {
          $(module.prototype.selector, context).each(function(index, element) {
            var instance = $(element).data(commonOptions.dataKey);
            if (instance) {
              instance.destroy();
            }
          });
        });
      } else {
        while (customInstances.length) {
          customInstances[0].destroy();
        }
      }
    }
  };

  // always export API to the global window object
  window.jcf = api;

  return api;
}));

/*!
 * JavaScript Custom Forms : Scrollbar Module
 *
 * Copyright 2014-2015 PSD2HTML - http://psd2html.com/jcf
 * Released under the MIT license (LICENSE.txt)
 *
 * Version: 1.1.3
 */
;(function($, window) {
  'use strict';

  jcf.addModule({
    name: 'Scrollable',
    selector: '.jcf-scrollable',
    plugins: {
      ScrollBar: ScrollBar
    },
    options: {
      mouseWheelStep: 150,
      handleResize: true,
      alwaysShowScrollbars: false,
      alwaysPreventMouseWheel: false,
      scrollAreaStructure: '<div class="jcf-scrollable-wrapper"></div>'
    },
    matchElement: function(element) {
      return element.is('.jcf-scrollable');
    },
    init: function() {
      this.initStructure();
      this.attachEvents();
      this.rebuildScrollbars();
    },
    initStructure: function() {
      // prepare structure
      this.doc = $(document);
      this.win = $(window);
      this.realElement = $(this.options.element);
      this.scrollWrapper = $(this.options.scrollAreaStructure).insertAfter(this.realElement);

      // set initial styles
      this.scrollWrapper.css('position', 'relative');
      this.realElement.css('overflow', 'hidden');
      this.vBarEdge = 0;
    },
    attachEvents: function() {
      // create scrollbars
      var self = this;
      this.vBar = new ScrollBar({
        holder: this.scrollWrapper,
        vertical: true,
        onScroll: function(scrollTop) {
          self.realElement.scrollTop(scrollTop);
        }
      });
      this.hBar = new ScrollBar({
        holder: this.scrollWrapper,
        vertical: false,
        onScroll: function(scrollLeft) {
          self.realElement.scrollLeft(scrollLeft);
        }
      });

      // add event handlers
      this.realElement.on('scroll', this.onScroll);
      if (this.options.handleResize) {
        this.win.on('resize orientationchange load', this.onResize);
      }

      // add pointer/wheel event handlers
      this.realElement.on('jcf-mousewheel', this.onMouseWheel);
      this.realElement.on('jcf-pointerdown', this.onTouchBody);
    },
    onScroll: function() {
      this.redrawScrollbars();
    },
    onResize: function() {
      // do not rebuild scrollbars if form field is in focus
      if (!$(document.activeElement).is(':input')) {
        this.rebuildScrollbars();
      }
    },
    onTouchBody: function(e) {
      if (e.pointerType === 'touch') {
        this.touchData = {
          scrollTop: this.realElement.scrollTop(),
          scrollLeft: this.realElement.scrollLeft(),
          left: e.pageX,
          top: e.pageY
        };
        this.doc.on({
          'jcf-pointermove': this.onMoveBody,
          'jcf-pointerup': this.onReleaseBody
        });
      }
    },
    onMoveBody: function(e) {
      var targetScrollTop,
        targetScrollLeft,
        verticalScrollAllowed = this.verticalScrollActive,
        horizontalScrollAllowed = this.horizontalScrollActive;

      if (e.pointerType === 'touch') {
        targetScrollTop = this.touchData.scrollTop - e.pageY + this.touchData.top;
        targetScrollLeft = this.touchData.scrollLeft - e.pageX + this.touchData.left;

        // check that scrolling is ended and release outer scrolling
        if (this.verticalScrollActive && (targetScrollTop < 0 || targetScrollTop > this.vBar.maxValue)) {
          verticalScrollAllowed = false;
        }
        if (this.horizontalScrollActive && (targetScrollLeft < 0 || targetScrollLeft > this.hBar.maxValue)) {
          horizontalScrollAllowed = false;
        }

        this.realElement.scrollTop(targetScrollTop);
        this.realElement.scrollLeft(targetScrollLeft);

        if (verticalScrollAllowed || horizontalScrollAllowed) {
          e.preventDefault();
        } else {
          this.onReleaseBody(e);
        }
      }
    },
    onReleaseBody: function(e) {
      if (e.pointerType === 'touch') {
        delete this.touchData;
        this.doc.off({
          'jcf-pointermove': this.onMoveBody,
          'jcf-pointerup': this.onReleaseBody
        });
      }
    },
    onMouseWheel: function(e) {
      var currentScrollTop = this.realElement.scrollTop(),
        currentScrollLeft = this.realElement.scrollLeft(),
        maxScrollTop = this.realElement.prop('scrollHeight') - this.embeddedDimensions.innerHeight,
        maxScrollLeft = this.realElement.prop('scrollWidth') - this.embeddedDimensions.innerWidth,
        extraLeft, extraTop, preventFlag;

      // check edge cases
      if (!this.options.alwaysPreventMouseWheel) {
        if (this.verticalScrollActive && e.deltaY) {
          if (!(currentScrollTop <= 0 && e.deltaY < 0) && !(currentScrollTop >= maxScrollTop && e.deltaY > 0)) {
            preventFlag = true;
          }
        }
        if (this.horizontalScrollActive && e.deltaX) {
          if (!(currentScrollLeft <= 0 && e.deltaX < 0) && !(currentScrollLeft >= maxScrollLeft && e.deltaX > 0)) {
            preventFlag = true;
          }
        }
        if (!this.verticalScrollActive && !this.horizontalScrollActive) {
          return;
        }
      }

      // prevent default action and scroll item
      if (preventFlag || this.options.alwaysPreventMouseWheel) {
        e.preventDefault();
      } else {
        return;
      }

      extraLeft = e.deltaX / 100 * this.options.mouseWheelStep;
      extraTop = e.deltaY / 100 * this.options.mouseWheelStep;

      this.realElement.scrollTop(currentScrollTop + extraTop);
      this.realElement.scrollLeft(currentScrollLeft + extraLeft);
    },
    setScrollBarEdge: function(edgeSize) {
      this.vBarEdge = edgeSize || 0;
      this.redrawScrollbars();
    },
    saveElementDimensions: function() {
      this.savedDimensions = {
        top: this.realElement.width(),
        left: this.realElement.height()
      };
      return this;
    },
    restoreElementDimensions: function() {
      if (this.savedDimensions) {
        this.realElement.css({
          width: this.savedDimensions.width,
          height: this.savedDimensions.height
        });
      }
      return this;
    },
    saveScrollOffsets: function() {
      this.savedOffsets = {
        top: this.realElement.scrollTop(),
        left: this.realElement.scrollLeft()
      };
      return this;
    },
    restoreScrollOffsets: function() {
      if (this.savedOffsets) {
        this.realElement.scrollTop(this.savedOffsets.top);
        this.realElement.scrollLeft(this.savedOffsets.left);
      }
      return this;
    },
    getContainerDimensions: function() {
      // save current styles
      var desiredDimensions,
        currentStyles,
        currentHeight,
        currentWidth;

      if (this.isModifiedStyles) {
        desiredDimensions = {
          width: this.realElement.innerWidth() + this.vBar.getThickness(),
          height: this.realElement.innerHeight() + this.hBar.getThickness()
        };
      } else {
        // unwrap real element and measure it according to CSS
        this.saveElementDimensions().saveScrollOffsets();
        this.realElement.insertAfter(this.scrollWrapper);
        this.scrollWrapper.detach();

        // measure element
        currentStyles = this.realElement.prop('style');
        currentWidth = parseFloat(currentStyles.width);
        currentHeight = parseFloat(currentStyles.height);

        // reset styles if needed
        if (this.embeddedDimensions && currentWidth && currentHeight) {
          this.isModifiedStyles |= (currentWidth !== this.embeddedDimensions.width || currentHeight !== this.embeddedDimensions.height);
          this.realElement.css({
            overflow: '',
            width: '',
            height: ''
          });
        }

        // calculate desired dimensions for real element
        desiredDimensions = {
          width: this.realElement.outerWidth(),
          height: this.realElement.outerHeight()
        };

        // restore structure and original scroll offsets
        this.scrollWrapper.insertAfter(this.realElement);
        this.realElement.css('overflow', 'hidden').prependTo(this.scrollWrapper);
        this.restoreElementDimensions().restoreScrollOffsets();
      }

      return desiredDimensions;
    },
    getEmbeddedDimensions: function(dimensions) {
      // handle scrollbars cropping
      var fakeBarWidth = this.vBar.getThickness(),
        fakeBarHeight = this.hBar.getThickness(),
        paddingWidth = this.realElement.outerWidth() - this.realElement.width(),
        paddingHeight = this.realElement.outerHeight() - this.realElement.height(),
        resultDimensions;

      if (this.options.alwaysShowScrollbars) {
        // simply return dimensions without custom scrollbars
        this.verticalScrollActive = true;
        this.horizontalScrollActive = true;
        resultDimensions = {
          innerWidth: dimensions.width - fakeBarWidth,
          innerHeight: dimensions.height - fakeBarHeight
        };
      } else {
        // detect when to display each scrollbar
        this.saveElementDimensions();
        this.verticalScrollActive = false;
        this.horizontalScrollActive = false;

        // fill container with full size
        this.realElement.css({
          width: dimensions.width - paddingWidth,
          height: dimensions.height - paddingHeight
        });

        this.horizontalScrollActive = this.realElement.prop('scrollWidth') > this.containerDimensions.width;
        this.verticalScrollActive = this.realElement.prop('scrollHeight') > this.containerDimensions.height;

        this.restoreElementDimensions();
        resultDimensions = {
          innerWidth: dimensions.width - (this.verticalScrollActive ? fakeBarWidth : 0),
          innerHeight: dimensions.height - (this.horizontalScrollActive ? fakeBarHeight : 0)
        };
      }
      $.extend(resultDimensions, {
        width: resultDimensions.innerWidth - paddingWidth,
        height: resultDimensions.innerHeight - paddingHeight
      });
      return resultDimensions;
    },
    rebuildScrollbars: function() {
      // resize wrapper according to real element styles
      this.containerDimensions = this.getContainerDimensions();
      this.embeddedDimensions = this.getEmbeddedDimensions(this.containerDimensions);

      // resize wrapper to desired dimensions
      this.scrollWrapper.css({
        width: this.containerDimensions.width,
        height: this.containerDimensions.height
      });

      // resize element inside wrapper excluding scrollbar size
      this.realElement.css({
        overflow: 'hidden',
        width: this.embeddedDimensions.width,
        height: this.embeddedDimensions.height
      });

      // redraw scrollbar offset
      this.redrawScrollbars();
    },
    redrawScrollbars: function() {
      var viewSize, maxScrollValue;

      // redraw vertical scrollbar
      if (this.verticalScrollActive) {
        viewSize = this.vBarEdge ? this.containerDimensions.height - this.vBarEdge : this.embeddedDimensions.innerHeight;
        maxScrollValue = Math.max(this.realElement.prop('offsetHeight'), this.realElement.prop('scrollHeight')) - this.vBarEdge;

        this.vBar.show().setMaxValue(maxScrollValue - viewSize).setRatio(viewSize / maxScrollValue).setSize(viewSize);
        this.vBar.setValue(this.realElement.scrollTop());
      } else {
        this.vBar.hide();
      }

      // redraw horizontal scrollbar
      if (this.horizontalScrollActive) {
        viewSize = this.embeddedDimensions.innerWidth;
        maxScrollValue = this.realElement.prop('scrollWidth');

        if (maxScrollValue === viewSize) {
          this.horizontalScrollActive = false;
        }
        this.hBar.show().setMaxValue(maxScrollValue - viewSize).setRatio(viewSize / maxScrollValue).setSize(viewSize);
        this.hBar.setValue(this.realElement.scrollLeft());
      } else {
        this.hBar.hide();
      }

      // set "touch-action" style rule
      var touchAction = '';
      if (this.verticalScrollActive && this.horizontalScrollActive) {
        touchAction = 'none';
      } else if (this.verticalScrollActive) {
        touchAction = 'pan-x';
      } else if (this.horizontalScrollActive) {
        touchAction = 'pan-y';
      }
      this.realElement.css('touchAction', touchAction);
    },
    refresh: function() {
      this.rebuildScrollbars();
    },
    destroy: function() {
      // remove event listeners
      this.win.off('resize orientationchange load', this.onResize);
      this.realElement.off({
        'jcf-mousewheel': this.onMouseWheel,
        'jcf-pointerdown': this.onTouchBody
      });
      this.doc.off({
        'jcf-pointermove': this.onMoveBody,
        'jcf-pointerup': this.onReleaseBody
      });

      // restore structure
      this.saveScrollOffsets();
      this.vBar.destroy();
      this.hBar.destroy();
      this.realElement.insertAfter(this.scrollWrapper).css({
        touchAction: '',
        overflow: '',
        width: '',
        height: ''
      });
      this.scrollWrapper.remove();
      this.restoreScrollOffsets();
    }
  });

  // custom scrollbar
  function ScrollBar(options) {
    this.options = $.extend({
      holder: null,
      vertical: true,
      inactiveClass: 'jcf-inactive',
      verticalClass: 'jcf-scrollbar-vertical',
      horizontalClass: 'jcf-scrollbar-horizontal',
      scrollbarStructure: '<div class="jcf-scrollbar"><div class="jcf-scrollbar-dec"></div><div class="jcf-scrollbar-slider"><div class="jcf-scrollbar-handle"></div></div><div class="jcf-scrollbar-inc"></div></div>',
      btnDecSelector: '.jcf-scrollbar-dec',
      btnIncSelector: '.jcf-scrollbar-inc',
      sliderSelector: '.jcf-scrollbar-slider',
      handleSelector: '.jcf-scrollbar-handle',
      scrollInterval: 300,
      scrollStep: 400 // px/sec
    }, options);
    this.init();
  }

  $.extend(ScrollBar.prototype, {
    init: function() {
      this.initStructure();
      this.attachEvents();
    },
    initStructure: function() {
      // define proporties
      this.doc = $(document);
      this.isVertical = !!this.options.vertical;
      this.sizeProperty = this.isVertical ? 'height' : 'width';
      this.fullSizeProperty = this.isVertical ? 'outerHeight' : 'outerWidth';
      this.invertedSizeProperty = this.isVertical ? 'width' : 'height';
      this.thicknessMeasureMethod = 'outer' + this.invertedSizeProperty.charAt(0).toUpperCase() + this.invertedSizeProperty.substr(1);
      this.offsetProperty = this.isVertical ? 'top' : 'left';
      this.offsetEventProperty = this.isVertical ? 'pageY' : 'pageX';

      // initialize variables
      this.value = this.options.value || 0;
      this.maxValue = this.options.maxValue || 0;
      this.currentSliderSize = 0;
      this.handleSize = 0;

      // find elements
      this.holder = $(this.options.holder);
      this.scrollbar = $(this.options.scrollbarStructure).appendTo(this.holder);
      this.btnDec = this.scrollbar.find(this.options.btnDecSelector);
      this.btnInc = this.scrollbar.find(this.options.btnIncSelector);
      this.slider = this.scrollbar.find(this.options.sliderSelector);
      this.handle = this.slider.find(this.options.handleSelector);

      // set initial styles
      this.scrollbar.addClass(this.isVertical ? this.options.verticalClass : this.options.horizontalClass).css({
        touchAction: this.isVertical ? 'pan-x' : 'pan-y',
        position: 'absolute'
      });
      this.slider.css({
        position: 'relative'
      });
      this.handle.css({
        touchAction: 'none',
        position: 'absolute'
      });
    },
    attachEvents: function() {
      this.bindHandlers();
      this.handle.on('jcf-pointerdown', this.onHandlePress);
      this.slider.add(this.btnDec).add(this.btnInc).on('jcf-pointerdown', this.onButtonPress);
    },
    onHandlePress: function(e) {
      if (e.pointerType === 'mouse' && e.button > 1) {
        return;
      } else {
        e.preventDefault();
        this.handleDragActive = true;
        this.sliderOffset = this.slider.offset()[this.offsetProperty];
        this.innerHandleOffset = e[this.offsetEventProperty] - this.handle.offset()[this.offsetProperty];

        this.doc.on('jcf-pointermove', this.onHandleDrag);
        this.doc.on('jcf-pointerup', this.onHandleRelease);
      }
    },
    onHandleDrag: function(e) {
      e.preventDefault();
      this.calcOffset = e[this.offsetEventProperty] - this.sliderOffset - this.innerHandleOffset;
      this.setValue(this.calcOffset / (this.currentSliderSize - this.handleSize) * this.maxValue);
      this.triggerScrollEvent(this.value);
    },
    onHandleRelease: function() {
      this.handleDragActive = false;
      this.doc.off('jcf-pointermove', this.onHandleDrag);
      this.doc.off('jcf-pointerup', this.onHandleRelease);
    },
    onButtonPress: function(e) {
      var direction, clickOffset;
      if (e.pointerType === 'mouse' && e.button > 1) {
        return;
      } else {
        e.preventDefault();
        if (!this.handleDragActive) {
          if (this.slider.is(e.currentTarget)) {
            // slider pressed
            direction = this.handle.offset()[this.offsetProperty] > e[this.offsetEventProperty] ? -1 : 1;
            clickOffset = e[this.offsetEventProperty] - this.slider.offset()[this.offsetProperty];
            this.startPageScrolling(direction, clickOffset);
          } else {
            // scrollbar buttons pressed
            direction = this.btnDec.is(e.currentTarget) ? -1 : 1;
            this.startSmoothScrolling(direction);
          }
          this.doc.on('jcf-pointerup', this.onButtonRelease);
        }
      }
    },
    onButtonRelease: function() {
      this.stopPageScrolling();
      this.stopSmoothScrolling();
      this.doc.off('jcf-pointerup', this.onButtonRelease);
    },
    startPageScrolling: function(direction, clickOffset) {
      var self = this,
        stepValue = direction * self.currentSize;

      // limit checker
      var isFinishedScrolling = function() {
        var handleTop = (self.value / self.maxValue) * (self.currentSliderSize - self.handleSize);

        if (direction > 0) {
          return handleTop + self.handleSize >= clickOffset;
        } else {
          return handleTop <= clickOffset;
        }
      };

      // scroll by page when track is pressed
      var doPageScroll = function() {
        self.value += stepValue;
        self.setValue(self.value);
        self.triggerScrollEvent(self.value);

        if (isFinishedScrolling()) {
          clearInterval(self.pageScrollTimer);
        }
      };

      // start scrolling
      this.pageScrollTimer = setInterval(doPageScroll, this.options.scrollInterval);
      doPageScroll();
    },
    stopPageScrolling: function() {
      clearInterval(this.pageScrollTimer);
    },
    startSmoothScrolling: function(direction) {
      var self = this, dt;
      this.stopSmoothScrolling();

      // simple animation functions
      var raf = window.requestAnimationFrame || function(func) {
          setTimeout(func, 16);
        };
      var getTimestamp = function() {
        return Date.now ? Date.now() : new Date().getTime();
      };

      // set animation limit
      var isFinishedScrolling = function() {
        if (direction > 0) {
          return self.value >= self.maxValue;
        } else {
          return self.value <= 0;
        }
      };

      // animation step
      var doScrollAnimation = function() {
        var stepValue = (getTimestamp() - dt) / 1000 * self.options.scrollStep;

        if (self.smoothScrollActive) {
          self.value += stepValue * direction;
          self.setValue(self.value);
          self.triggerScrollEvent(self.value);

          if (!isFinishedScrolling()) {
            dt = getTimestamp();
            raf(doScrollAnimation);
          }
        }
      };

      // start animation
      self.smoothScrollActive = true;
      dt = getTimestamp();
      raf(doScrollAnimation);
    },
    stopSmoothScrolling: function() {
      this.smoothScrollActive = false;
    },
    triggerScrollEvent: function(scrollValue) {
      if (this.options.onScroll) {
        this.options.onScroll(scrollValue);
      }
    },
    getThickness: function() {
      return this.scrollbar[this.thicknessMeasureMethod]();
    },
    setSize: function(size) {
      // resize scrollbar
      var btnDecSize = this.btnDec[this.fullSizeProperty](),
        btnIncSize = this.btnInc[this.fullSizeProperty]();

      // resize slider
      this.currentSize = size;
      this.currentSliderSize = size - btnDecSize - btnIncSize;
      this.scrollbar.css(this.sizeProperty, size);
      this.slider.css(this.sizeProperty, this.currentSliderSize);
      this.currentSliderSize = this.slider[this.sizeProperty]();

      // resize handle
      this.handleSize = Math.round(this.currentSliderSize * this.ratio);
      this.handle.css(this.sizeProperty, this.handleSize);
      this.handleSize = this.handle[this.fullSizeProperty]();

      return this;
    },
    setRatio: function(ratio) {
      this.ratio = ratio;
      return this;
    },
    setMaxValue: function(maxValue) {
      this.maxValue = maxValue;
      this.setValue(Math.min(this.value, this.maxValue));
      return this;
    },
    setValue: function(value) {
      this.value = value;
      if (this.value < 0) {
        this.value = 0;
      } else if (this.value > this.maxValue) {
        this.value = this.maxValue;
      }
      this.refresh();
    },
    setPosition: function(styles) {
      this.scrollbar.css(styles);
      return this;
    },
    hide: function() {
      this.scrollbar.detach();
      return this;
    },
    show: function() {
      this.scrollbar.appendTo(this.holder);
      return this;
    },
    refresh: function() {
      // recalculate handle position
      if (this.value === 0 || this.maxValue === 0) {
        this.calcOffset = 0;
      } else {
        this.calcOffset = (this.value / this.maxValue) * (this.currentSliderSize - this.handleSize);
      }
      this.handle.css(this.offsetProperty, this.calcOffset);

      // toggle inactive classes
      this.btnDec.toggleClass(this.options.inactiveClass, this.value === 0);
      this.btnInc.toggleClass(this.options.inactiveClass, this.value === this.maxValue);
      this.scrollbar.toggleClass(this.options.inactiveClass, this.maxValue === 0);
    },
    destroy: function() {
      // remove event handlers and scrollbar block itself
      this.btnDec.add(this.btnInc).off('jcf-pointerdown', this.onButtonPress);
      this.handle.off('jcf-pointerdown', this.onHandlePress);
      this.doc.off('jcf-pointermove', this.onHandleDrag);
      this.doc.off('jcf-pointerup', this.onHandleRelease);
      this.doc.off('jcf-pointerup', this.onButtonRelease);
      this.stopSmoothScrolling();
      this.stopPageScrolling();
      this.scrollbar.remove();
    }
  });

}(jQuery, this));
