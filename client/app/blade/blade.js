'use strict';

angular.module('windmanagerApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('blade', {
        url: '/blade/:id',
        template: '<blade></blade>',
        authenticate: true,
        data: {
          bodyClass: ''
        }
      });
  });
