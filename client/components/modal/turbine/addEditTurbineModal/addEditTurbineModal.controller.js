'use strict';

(function() {
  class AddEditTurbineModalCtrl {
    constructor($scope, $uibModalInstance, Site, Turbine, options, usSpinnerService, Util) {
      this.Util = Util;
      this.Site = Site;
      this.Turbine = Turbine;
      this.usSpinnerService = usSpinnerService;
      this.$uibModalInstance = $uibModalInstance;
      this.isDisabled = false;
      this.errors = {};
      this.popup1 = {
        opened: false
      };
      this.popup2 = {
        opened: false
      };
      this.options = options;
      this.submitted = false;

      this.turbine = {
        site: this.options.site,
        make: null,
        model: null,
        serial: null,
        capacity: null,
        sitePosition: null,
        type: null,
        manufactured: null,
        commisioning: null
      };

      $scope.$on('$stateChangeStart', function() {
        $uibModalInstance.dismiss();
      });
    }

    saveTurbine(form) {
      this.submitted = true;
      this.serverError = null;
      if (form.$valid) {
        this.isDisabled = true;
        this.startSpin('spinner-1');
        this.Turbine.save(this.turbine).$promise
          .then(turbine=> {
            this.stopSpin('spinner-1');
            console.log(turbine);
            this.$uibModalInstance.close(turbine);
          })
          .catch(err=> {
            this.isDisabled = false;
            this.errors = {};
            this.stopSpin('spinner-1');
            this.handleError(err, form);
          });
      }
    }

    handleError(err, form) {
      if (err && err.data && err.data.meta && err.data.meta.error_message) {
        if (err.data.meta.code && err.data.meta.code != 422 && !this.Util.isArray(err.data.meta.error_message)) {
          this.serverError = err.data.meta.error_message || 'Internal Server Error';
        } else {
          err.data.meta.error_message.forEach(errorMesssage=> {
            for (let fieldName in errorMesssage) {
              if (form[fieldName]) {
                form[fieldName].$setValidity('mongoose', false);
                this.errors[fieldName] = errorMesssage[fieldName];
              }
            }
          })
        }
      } else {
        this.serverError = 'Internal Server Error';
        console.log(err);
      }
    }

    openManufacturedDate() {
      this.popup1.opened = true;
    }

    openCommisioningDate() {
      this.popup2.opened = true;
    }

    cancelSave() {
      this.$uibModalInstance.dismiss('cancel');
    }

    startSpin(spinner) {
      this.usSpinnerService.spin(spinner);
    }

    stopSpin(spinner) {
      this.usSpinnerService.stop(spinner);
    }
  }
  angular.module('windmanagerApp')
    .controller('AddEditTurbineModalCtrl', AddEditTurbineModalCtrl);
})();
