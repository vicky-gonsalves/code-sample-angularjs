'use strict';

import _ from 'lodash';
import Blade from './blade.model';
import * as TurbineCtrl from '../turbine/turbine.controller';
import * as SiteCtrl from '../site/site.controller';

const validator = require('validator');
const Promise = require('bluebird');
const ResponseHelpers = require('../../components/responseHelpers/responseHelpers');
Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});

function sanitizeEntities(data, toValidate = []) {
  return new Promise((resolve, reject)=> {
    try {
      let sanitizedData = JSON.parse(JSON.stringify(data));
      if (data.turbine && toValidate.indexOf('turbine') > -1) {
        sanitizedData.turbine = validator.trim(data.turbine);
      }
      if (data.serial && toValidate.indexOf('serial') > -1) {
        sanitizedData.serial = validator.trim(data.serial);
      }
      if (data.make && toValidate.indexOf('make') > -1) {
        sanitizedData.make = validator.trim(data.make);
      }
      if (data.model && toValidate.indexOf('model') > -1) {
        sanitizedData.model = validator.trim(data.model);
      }
      if (data.material && toValidate.indexOf('material') > -1) {
        sanitizedData.material = validator.trim(data.material);
      }
      if (data.surface && toValidate.indexOf('surface') > -1) {
        sanitizedData.surface = validator.trim(data.surface);
      }
      if (data.type && toValidate.indexOf('type') > -1) {
        sanitizedData.type = validator.trim(data.type);
      }
      return resolve(sanitizedData);
    } catch (e) {
      console.log('Error in blade.controller.sanitizeEntities 1');
      return reject(e);
    }
  })
}

function validateEntities(toValidate = []) {
  return function(data) {
    return new Promise((resolve, reject)=> {
      if (data) {

        let errors = {type: 'InvalidParams', code: 422, msg: []};
        //Validation
        if (toValidate.indexOf('turbine') > -1) {
          if (data.turbine && !validator.isMongoId(data.turbine)) {
            errors.msg.push({turbine: 'Invalid turbine id'});
          }
        }
        if (toValidate.indexOf('serial') > -1) {
          if (!(data.serial) || validator.isNull(data.serial)) {
            errors.msg.push({serial: 'Serial is required'});
          } else if (data.serial && !validator.isLength(data.serial, {min: 1, max: 150})) {
            errors.msg.push({serial: 'Serial must be between 1 to 150 characters long'});
          }
        }

        if (toValidate.indexOf('make') > -1) {
          if (!(data.make) || validator.isNull(data.make)) {
            errors.msg.push({make: 'Make is required'});
          } else if (data.make && !validator.isLength(data.make, {min: 1, max: 150})) {
            errors.msg.push({make: 'Make must be between 1 to 150 characters long'});
          }
        }

        if (toValidate.indexOf('model') > -1) {
          if (!(data.model) || validator.isNull(data.model)) {
            errors.msg.push({model: 'Model is required'});
          } else if (data.model && !validator.isLength(data.model, {min: 1, max: 150})) {
            errors.msg.push({model: 'Model must be between 1 to 150 characters long'});
          }
        }

        if (toValidate.indexOf('position') > -1) {
          if (!(data.position) || validator.isNull(data.position.toString())) {
            errors.msg.push({position: 'Position is required'});
          } else if (data.position && !validator.isNumeric(data.position.toString())) {
            errors.msg.push({position: 'Position must be number'});
          }
        }

        if (toValidate.indexOf('material') > -1) {
          if (!(data.material) || validator.isNull(data.material)) {
            errors.msg.push({material: 'Material is required'});
          } else if (data.material && !validator.isLength(data.material, {min: 1, max: 150})) {
            errors.msg.push({material: 'Material must be between 1 to 150 characters long'});
          }
        }

        if (toValidate.indexOf('diameter') > -1) {
          if (!(data.diameter) || validator.isNull(data.diameter.toString())) {
            errors.msg.push({diameter: 'Diameter is required'});
          } else if (data.diameter && !validator.isNumeric(data.diameter.toString())) {
            errors.msg.push({diameter: 'Diameter must be number'});
          }
        }

        if (toValidate.indexOf('surface') > -1) {
          if (!(data.surface) || validator.isNull(data.surface)) {
            errors.msg.push({surface: 'Surface is required'});
          } else if (data.surface && !validator.isLength(data.surface, {min: 1, max: 150})) {
            errors.msg.push({surface: 'Surface must be between 1 to 150 characters long'});
          }
        }

        if (toValidate.indexOf('type') > -1) {
          if (!(data.type) || validator.isNull(data.type)) {
            errors.msg.push({type: 'Type is required'});
          } else if (data.type && !validator.isLength(data.type, {min: 1, max: 150})) {
            errors.msg.push({type: 'Type must be between 1 to 150 characters long'});
          }
        }

        if (toValidate.indexOf('length') > -1) {
          if (!(data.length) || validator.isNull(data.length.toString())) {
            errors.msg.push({length: 'Length is required'});
          } else if (data.length && !validator.isNumeric(data.length.toString())) {
            errors.msg.push({length: 'Length must be number'});
          }
        }

        if (toValidate.indexOf('condition') > -1) {
          if (!(data.condition) || validator.isNull(data.condition.toString())) {
            errors.msg.push({condition: 'Condition is required'});
          } else if (data.condition && !validator.isNumeric(data.condition.toString())) {
            errors.msg.push({condition: 'Condition must be number'});
          }
        }

        if (errors.msg.length > 0) {
          console.log('Error in blade.controller.validateEntities 1');
          return reject(errors);
        } else {
          return resolve(data);
        }
      } else {
        console.log('Error in blade.controller.validateEntities 2');
        let err = {type: 'EntityNotFound', code: 404, msg: 'Entity not found.'};
        return reject(err);
      }
    });
  }
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity() {
  return function(entity) {
    return new Promise((resolve, reject)=> {
      if (entity) {
        return entity.remove()
          .then(() => {
            return resolve();
          })
          .catch(err=> {
            console.log('Error in blade.controller.removeEntity 1');
            return reject(err);
          })
      } else {
        console.log('Error in blade.controller.removeEntity 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'blade not found'});
      }
    });
  };
}

function addBladeInTurbine(turbine) {
  return function(entity) {
    return new Promise((resolve, reject)=> {
      if (entity) {
        return TurbineCtrl.addBladeInTurbine(turbine, entity._id)
          .then(()=> {
            return resolve(entity);
          })
          .catch(err=> {
            console.log('Error in blade.controller.addBladeInTurbine 1');
            return reject(err);
          })
      } else {
        console.log('Error in blade.controller.addBladeInTurbine 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'Entity not found.'});
      }
    })
  }
}

function getSite() {
  return function(blade) {
    return new Promise((resolve, reject)=> {
      if (blade) {
        return SiteCtrl.getSiteByTurbineId(blade.turbine._id)
          .then(site=> {
            blade.turbine.site = site;
            return resolve(blade);
          })
          .catch(err=> {
            console.log('Error in blade.controller.getSite 1');
            return reject(err);
          })
      } else {
        console.log('Error in blade.controller.getSite 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'Entity not found.'});
      }
    })
  }
}

function checkIfTurbineExists(turbineId) {
  return new Promise((resolve, reject)=> {
    if (turbineId) {
      return TurbineCtrl.getTurbineById(turbineId)
        .then(turbine=> {
          if (turbine) {
            return resolve(true);
          }
          console.log('Error in blade.controller.checkIfTurbineExists 1');
          let err = {type: 'EntityNotFound', code: 404, msg: 'Turbine not found'};
          return reject(err);
        })
        .catch(err=> {
          console.log('Error in blade.controller.checkIfTurbineExists 2');
          return reject(err);
        })
    } else {
      console.log('Error in blade.controller.checkIfTurbineExists 3');
      return reject({type: 'MissingParams', code: 422, msg: [{turbine: 'Turbine is required'}]});
    }
  })
}

function checkIfPositionExists(turbineId, position) {
  return new Promise((resolve, reject)=> {
    if (turbineId && position) {
      return Blade.findOne({'turbine._id': turbineId, position: position})
        .then(blade=> {
          if (!blade) {
            return resolve(true);
          }
          console.log('Error in blade.controller.checkIfPositionExists 1');
          let err = {type: 'AlreadyExists', code: 409, msg: 'Blade already exists on the position.'};
          return reject(err);
        })
        .catch(err=> {
          console.log('Error in blade.controller.checkIfPositionExists 2');
          return reject(err);
        })
    } else {
      console.log('Error in blade.controller.checkIfPositionExists 3');
      return reject({type: 'MissingParams', code: 422, msg: [{turbine: 'Turbine/position is required'}]});
    }
  })
}

/*Default Root functions*/
// Gets a list of Blades
export function index(req, res) {
  return Blade.find()
    .populate('turbine')
    .exec()
    .sort({createdAt: 1})
    .exec()
    .then(ResponseHelpers.respondWithResult(res))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Gets a single Blade from the DB
export function show(req, res) {
  return Blade.findById(req.params.id)
    .populate('turbine')
    .lean()
    .exec()
    .then(getSite())
    .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Blade not found.'}))
    .then(ResponseHelpers.respondWithResult(res))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Creates a new Blade in the DB
export function create(req, res) {
  if (req.body.createdAt) {
    delete req.body.createdAt;
  }
  if (req.body.updatedAt) {
    delete req.body.updatedAt;
  }
  if (req.body.createdBy) {
    delete req.body.createdBy;
  }
  if (req.body.updatedBy) {
    delete req.body.updatedBy;
  }
  let toValidate = ['turbine', 'serial', 'make', 'model', 'position', 'material', 'diameter', 'surface', 'type', 'length', 'condition'];
  return checkIfTurbineExists(req.body.turbine)
    .then(()=> {
      return sanitizeEntities(req.body, toValidate)
        .then(validateEntities(toValidate))
        .then(data=> {
          return checkIfPositionExists(data.turbine, data.position)
            .then(()=> {
              data.createdBy = req.user._id;
              return Blade.create(data)
                .then(addBladeInTurbine(data.turbine))
                .then(ResponseHelpers.respondWithResult(res, 201))
                .catch(ResponseHelpers.respondWithErrorEntity(res));
            })
            .catch(ResponseHelpers.respondWithErrorEntity(res));
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Updates an existing Blade in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  if (req.body.createdAt) {
    delete req.body.createdAt;
  }
  if (req.body.updatedAt) {
    delete req.body.updatedAt;
  }
  if (req.body.createdBy) {
    delete req.body.createdBy;
  }
  if (req.body.updatedBy) {
    delete req.body.updatedBy;
  }
  if (req.body.turbine) {
    delete req.body.turbine;
  }
  let toValidate = ['turbine', 'serial', 'make', 'model', 'position', 'material', 'diameter', 'surface', 'type', 'length', 'condition'];
  return sanitizeEntities(req.body, toValidate)
    .then(validateEntities(toValidate))
    .then(data=> {
      data.updatedBy = req.user._id;
      data.updatedAt = Date.now();
      return Blade.findById(req.params.id).exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Blade not found.'}))
        .then(saveUpdates(data))
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Deletes a Blade from the DB
export function destroy(req, res) {
  return Blade.findById(req.params.id).exec()
    .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Blade not found.'}))
    .then(removeEntity())
    .then(ResponseHelpers.respondWithResult(res, 204))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}
