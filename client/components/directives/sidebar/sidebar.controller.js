'use strict';

(function() {

  class SidebarCtrl {
    constructor($state) {
      this.$state = $state;

      this.routeMap = {dashboard: ['main'], manage: ['turbine', 'blade']};
    }

    isActive(route) {
      if (route) {
        return this.routeMap[route].indexOf(this.$state.current.name) > -1;
      }
      return false;
    }
  }

  angular.module('windmanagerApp')
    .controller('SidebarCtrl', SidebarCtrl);
})();
