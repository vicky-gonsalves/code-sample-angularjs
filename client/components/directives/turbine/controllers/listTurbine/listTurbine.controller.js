'use strict';

(function() {

  class ListTurbineCtrl {
    constructor($scope) {
      this.$scope = $scope;
    }
  }

  angular.module('windmanagerApp')
    .controller('ListTurbineCtrl', ListTurbineCtrl);

})();
