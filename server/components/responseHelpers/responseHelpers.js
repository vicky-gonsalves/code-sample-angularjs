"use strict";
const mongoose = require('mongoose');
const Promise = require('bluebird');
Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});

function isArray(value) {
  return !!(value instanceof Array);
}

/*Response Helpers*/
export function respondWithResult(res, statusCode = 200) {
  return function(entity) {
    if (entity) {
      return res.status(statusCode).json({"meta": {"code": statusCode}, "data": entity});
    } else {
      return res.status(statusCode).end();
    }
  };
}

export function respondWithCustomResult(res, result, statusCode = 200) {
  return res.status(statusCode).json({"meta": {"code": statusCode}, "data": result});
}

export function respondWithPaginatedResult(res, statusCode = 200) {
  return function(entity) {
    if (entity && entity.__count) {
      let count = entity.__count;
      delete entity.__count;
      return res.status(statusCode).json({"meta": {"code": statusCode}, "data": entity, "count": count});
    } else {
      console.log('Error in responseHelpers.respondWithPaginatedResult 1');
      let err = {type: 'EntityNotFound', code: 404, msg: 'entity or count is not available'};
      return respondWithCustomError(res, err);
    }
  };
}

export function respondWithPaginatedAndPageResult(res, statusCode = 200) {
  return function(entity) {
    if (entity && entity.__count && entity.__page) {
      let count = entity.__count;
      let page = entity.__page;
      delete entity.__count;
      delete entity.__page;
      return res.status(statusCode).json({"meta": {"code": statusCode}, "data": entity, "count": count, "page": page});
    } else {
      console.log('Error in responseHelpers.respondWithPaginatedResult 1');
      let err = {type: 'EntityNotFound', code: 404, msg: 'entity or count is not available'};
      return respondWithCustomError(res, err);
    }
  };
}

export function respondWithCustomError(res, err) {
  return res.status(err.code).json({"meta": {"error_type": err.type, "code": err.code, "error_message": err.msg}});
}

export function respondWithErrorEntity(res) {
  return function(err) {
    console.log(err);
    return res.status(err.code).json({"meta": {"error_type": err.type, "code": err.code, "error_message": err.msg}});
  }
}

export function handleEntityNotFound(err) {
  return function(entity) {
    return new Promise((resolve, reject)=> {
      if (entity) {
        return resolve(entity);
      } else {
        console.log('Error in responseHelpers.handleEntityNotFound 1');
        return reject(err);
      }
    });
  };
}

export function stripResults(fieldsToStrip) {
  return function(entity) {
    return new Promise((resolve, reject)=> {
      if (entity) {
        if (isArray(entity)) {
          let strippedData = entity.map(data=> {
            fieldsToStrip.forEach(key=> {
              delete data[key];
            });
          });
          return resolve(strippedData);
        } else {
          let data = JSON.parse(JSON.stringify(entity));
          fieldsToStrip.forEach(key=> {
            delete data[key];
          });
          return resolve(data);
        }
      } else {
        console.log('Error in responseHelpers.stripResults 1');
        let err = {type: 'EntityNotFound', code: 404, msg: 'entity is not available'};
        return reject(err);
      }
    });
  };
}

export function handleValidObjectIds(ids) {
  return new Promise((resolve, reject)=> {
    let errors = {type: 'InvalidParams', code: 422, msg: []};
    ids.forEach(id=> {
      if (!mongoose.Types.ObjectId.isValid(id.value)) {
        let err = {type: 'InvalidParams', code: 422, msg: `${id.type + ' ' || ''}id is not valid`};
        errors.msg.push(err);
      }
    });
    if (errors.msg.length > 0) {
      console.log('Error in responseHelpers.handleValidObjectId 1');
      return reject(errors);
    } else {
      return resolve();
    }
  });
}
