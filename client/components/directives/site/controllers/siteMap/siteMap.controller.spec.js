'use strict';

describe('Controller: SiteMapCtrl', function () {

  // load the controller's module
  beforeEach(module('windmanagerApp'));

  var SiteMapCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SiteMapCtrl = $controller('SiteMapCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
