'use strict';

var app = require('../..');
import request from 'supertest';

var newTurbine;

describe('Turbine API:', function() {

  describe('GET /api/turbines', function() {
    var turbines;

    beforeEach(function(done) {
      request(app)
        .get('/api/turbines')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          turbines = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      turbines.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/turbines', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/turbines')
        .send({
          name: 'New Turbine',
          info: 'This is the brand new turbine!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newTurbine = res.body;
          done();
        });
    });

    it('should respond with the newly created turbine', function() {
      newTurbine.name.should.equal('New Turbine');
      newTurbine.info.should.equal('This is the brand new turbine!!!');
    });

  });

  describe('GET /api/turbines/:id', function() {
    var turbine;

    beforeEach(function(done) {
      request(app)
        .get('/api/turbines/' + newTurbine._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          turbine = res.body;
          done();
        });
    });

    afterEach(function() {
      turbine = {};
    });

    it('should respond with the requested turbine', function() {
      turbine.name.should.equal('New Turbine');
      turbine.info.should.equal('This is the brand new turbine!!!');
    });

  });

  describe('PUT /api/turbines/:id', function() {
    var updatedTurbine;

    beforeEach(function(done) {
      request(app)
        .put('/api/turbines/' + newTurbine._id)
        .send({
          name: 'Updated Turbine',
          info: 'This is the updated turbine!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedTurbine = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedTurbine = {};
    });

    it('should respond with the updated turbine', function() {
      updatedTurbine.name.should.equal('Updated Turbine');
      updatedTurbine.info.should.equal('This is the updated turbine!!!');
    });

  });

  describe('DELETE /api/turbines/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/turbines/' + newTurbine._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when turbine does not exist', function(done) {
      request(app)
        .delete('/api/turbines/' + newTurbine._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
