'use strict';

(function() {
  function FindingResource($resource) {
    return $resource('api/v1/findings/:id/:controller/:page/:limit/:sortBy/:order', {
      id: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }

  angular.module('windmanagerApp')
    .factory('Finding', FindingResource);

})();
