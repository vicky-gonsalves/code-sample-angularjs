'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var findingCtrlStub = {
  index: 'findingCtrl.index',
  show: 'findingCtrl.show',
  create: 'findingCtrl.create',
  update: 'findingCtrl.update',
  destroy: 'findingCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var findingIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './finding.controller': findingCtrlStub
});

describe('Finding API Router:', function() {

  it('should return an express router instance', function() {
    findingIndex.should.equal(routerStub);
  });

  describe('GET /api/findings', function() {

    it('should route to finding.controller.index', function() {
      routerStub.get
        .withArgs('/', 'findingCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/findings/:id', function() {

    it('should route to finding.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'findingCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/findings', function() {

    it('should route to finding.controller.create', function() {
      routerStub.post
        .withArgs('/', 'findingCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/findings/:id', function() {

    it('should route to finding.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'findingCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/findings/:id', function() {

    it('should route to finding.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'findingCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/findings/:id', function() {

    it('should route to finding.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'findingCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
