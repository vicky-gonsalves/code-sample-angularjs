'use strict';

describe('Controller: ViewTurbineCtrl', function () {

  // load the controller's module
  beforeEach(module('windmanagerApp'));

  var ViewTurbineCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ViewTurbineCtrl = $controller('ViewTurbineCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
