'use strict';

import _ from 'lodash';
import Finding from './finding.model';

const validator = require('validator');
const Promise = require('bluebird');
const ResponseHelpers = require('../../components/responseHelpers/responseHelpers');
Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});

function sanitizeEntities(data, toValidate = []) {
  return new Promise((resolve, reject)=> {
    try {
      let sanitizedData = JSON.parse(JSON.stringify(data));
      if (data.make && toValidate.indexOf('make') > -1) {
        sanitizedData.make = validator.trim(data.make);
      }
      if (data.model && toValidate.indexOf('model') > -1) {
        sanitizedData.model = validator.trim(data.model);
      }
      if (data.serial && toValidate.indexOf('serial') > -1) {
        sanitizedData.serial = validator.trim(data.serial);
      }
      if (data.type && toValidate.indexOf('type') > -1) {
        sanitizedData.type = validator.trim(data.type);
      }
      if (data.manufactured && toValidate.indexOf('manufactured') > -1) {
        sanitizedData.manufactured = validator.trim(data.manufactured);
      }
      if (data.commisioning && toValidate.indexOf('commisioning') > -1) {
        sanitizedData.commisioning = validator.trim(data.commisioning);
      }
      return resolve(sanitizedData);
    } catch (e) {
      console.log('Error in finding.controller.sanitizeEntities 1');
      return reject(e);
    }
  })
}

function validateEntities(toValidate = []) {
  return function(data) {
    return new Promise((resolve, reject)=> {
      if (data) {

        let errors = {type: 'InvalidParams', code: 422, msg: []};
        //Validation
        if (toValidate.indexOf('make') > -1) {
          if (!(data.make) || validator.isNull(data.make)) {
            errors.msg.push({make: 'Make is required'});
          } else if (data.make && !validator.isLength(data.make, {min: 1, max: 150})) {
            errors.msg.push({make: 'Make must be between 1 to 150 characters long'});
          }
        }

        if (toValidate.indexOf('model') > -1) {
          if (!(data.model) || validator.isNull(data.model)) {
            errors.msg.push({model: 'Model is required'});
          } else if (data.model && !validator.isLength(data.model, {min: 1, max: 150})) {
            errors.msg.push({model: 'Model must be between 1 to 150 characters long'});
          }
        }


        if (toValidate.indexOf('serial') > -1) {
          if (!(data.serial) || validator.isNull(data.serial)) {
            errors.msg.push({serial: 'Serial is required'});
          } else if (data.serial && !validator.isLength(data.serial, {min: 1, max: 150})) {
            errors.msg.push({serial: 'Serial must be between 1 to 150 characters long'});
          }
        }

        if (toValidate.indexOf('capacity') > -1) {
          if (!(data.capacity)) {
            errors.msg.push({capacity: 'Capacity is required'});
          }
        }

        if (toValidate.indexOf('findingPosition') > -1) {
          if (!(data.findingPosition)) {
            errors.msg.push({findingPosition: 'finding Position is required'});
          }
        }

        if (toValidate.indexOf('type') > -1) {
          if (!(data.type) || validator.isNull(data.type)) {
            errors.msg.push({type: 'Type is required'});
          } else if (data.type && !validator.isLength(data.type, {min: 1, max: 150})) {
            errors.msg.push({type: 'Type must be between 1 to 150 characters long'});
          }
        }

        if (toValidate.indexOf('manufactured') > -1) {
          if (!(data.manufactured) || validator.isNull(data.manufactured)) {
            errors.msg.push({manufactured: 'Manufactured is required'});
          } else if (data.manufactured && !validator.isDate(data.manufactured)) {
            errors.msg.push({manufactured: 'Manufactured must be a valid date'});
          }
        }

        if (toValidate.indexOf('commisioning') > -1) {
          if (!(data.commisioning) || validator.isNull(data.commisioning)) {
            errors.msg.push({commisioning: 'Commisioning date is required'});
          } else if (data.commisioning && !validator.isDate(data.commisioning)) {
            errors.msg.push({commisioning: 'Commisioning date must be a valid date'});
          }
        }

        if (errors.msg.length > 0) {
          console.log('Error in finding.controller.validateEntities 1');
          return reject(errors);
        } else {
          return resolve(data);
        }
      } else {
        console.log('Error in finding.controller.validateEntities 2');
        let err = {type: 'EntityNotFound', code: 404, msg: 'Entity not found.'};
        return reject(err);
      }
    });
  }
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity() {
  return function(entity) {
    return new Promise((resolve, reject)=> {
      if (entity) {
        return entity.remove()
          .then(() => {
            return resolve();
          })
          .catch(err=> {
            console.log('Error in finding.controller.removeEntity 1');
            return reject(err);
          })
      } else {
        console.log('Error in finding.controller.removeEntity 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'finding not found'});
      }
    });
  };
}


/*Default Root functions*/
// Gets a list of Findings
export function index(req, res) {
  return Finding.find().exec()
    .populate('blade')
    .sort({createdAt: 1})
    .exec()
    .then(ResponseHelpers.respondWithResult(res))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Gets a single Finding from the DB
export function show(req, res) {
  return Finding.findById(req.params.id).exec()
    .populate('blade')
    .lean()
    .exec()
    // .then(getSite())
    .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Finding not found.'}))
    .then(ResponseHelpers.respondWithResult(res))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Creates a new Finding in the DB
export function create(req, res) {
  if (req.body.createdAt) {
    delete req.body.createdAt;
  }
  if (req.body.updatedAt) {
    delete req.body.updatedAt;
  }
  if (req.body.createdBy) {
    delete req.body.createdBy;
  }
  if (req.body.updatedBy) {
    delete req.body.updatedBy;
  }
  if (req.body.turbine) {
    delete req.body.turbine;
  }
  let toValidate = ['make', 'model', 'serial', 'capacity', 'sitePosition', 'type', 'manufactured', 'commisioning'];
  return sanitizeEntities(req.body, toValidate)
    .then(validateEntities(toValidate))
    .then(data=> {
      data.createdBy = req.user._id;
      return Finding.create(data)
      // .then(addTurbineInSite(data.site))
        .then(ResponseHelpers.respondWithResult(res, 201))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Updates an existing Finding in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  if (req.body.createdAt) {
    delete req.body.createdAt;
  }
  if (req.body.updatedAt) {
    delete req.body.updatedAt;
  }
  if (req.body.createdBy) {
    delete req.body.createdBy;
  }
  if (req.body.updatedBy) {
    delete req.body.updatedBy;
  }
  if (req.body.turbine) {
    delete req.body.turbine;
  }
  let toValidate = ['make', 'model', 'serial', 'capacity', 'sitePosition', 'type', 'manufactured', 'commisioning'];
  return sanitizeEntities(req.body, toValidate)
    .then(validateEntities(toValidate))
    .then(data=> {
      data.updatedBy = req.user._id;
      data.updatedAt = Date.now();
      return Finding.findById(req.params.id).exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Finding not found.'}))
        .then(saveUpdates(data))
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Deletes a Finding from the DB
export function destroy(req, res) {
  return Finding.findById(req.params.id).exec()
    .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Finding not found.'}))
    .then(removeEntity())
    .then(ResponseHelpers.respondWithResult(res, 204))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}
