'use strict';

describe('Service: finding', function () {

  // load the service's module
  beforeEach(module('windmanagerApp'));

  // instantiate service
  var finding;
  beforeEach(inject(function (_finding_) {
    finding = _finding_;
  }));

  it('should do something', function () {
    expect(!!finding).toBe(true);
  });

});
