'use strict';

describe('Directive: listSite', function () {

  // load the directive's module and view
  beforeEach(module('windmanagerApp'));
  beforeEach(module('components/directives/listSite/listSite.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<list-site></list-site>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the listSite directive');
  }));
});
