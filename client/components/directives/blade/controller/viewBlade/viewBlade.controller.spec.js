'use strict';

describe('Controller: ViewBladeCtrl', function () {

  // load the controller's module
  beforeEach(module('windmanagerApp'));

  var ViewBladeCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ViewBladeCtrl = $controller('ViewBladeCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
