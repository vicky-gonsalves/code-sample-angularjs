'use strict';

describe('Service: blade', function () {

  // load the service's module
  beforeEach(module('windmanagerApp'));

  // instantiate service
  var blade;
  beforeEach(inject(function (_blade_) {
    blade = _blade_;
  }));

  it('should do something', function () {
    expect(!!blade).toBe(true);
  });

});
