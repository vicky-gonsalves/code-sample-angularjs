'use strict';

describe('Controller: AddEditTurbineModalCtrl', function () {

  // load the controller's module
  beforeEach(module('windmanagerApp'));

  var AddEditTurbineModalCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AddEditTurbineModalCtrl = $controller('AddEditTurbineModalCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
