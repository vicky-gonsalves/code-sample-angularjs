'use strict';

(function() {
  class AddEditSiteModalCtrl {
    constructor($scope, $uibModalInstance, Site, options, usSpinnerService, Util) {
      this.Util = Util;
      this.Site = Site;
      this.usSpinnerService = usSpinnerService;
      this.$uibModalInstance = $uibModalInstance;
      this.isDisabled = false;
      this.errors = {};
      this.site = {
        name: null,
        location: {
          addressLine1: null,
          addressLine2: null,
          addressLine3: null,
          addressLine4: null,
          addressLine5: null,
        }
      };
      this.options = options;
      this.submitted = false;

      $scope.$on('$stateChangeStart', function() {
        $uibModalInstance.dismiss();
      });
    }

    saveSite(form) {
      this.submitted = true;
      this.serverError = null;
      if (form.$valid) {
        this.isDisabled = true;
        this.startSpin('spinner-1');
        this.Site.save(this.site).$promise
          .then(site=> {
            this.stopSpin('spinner-1');
            console.log(site);
            this.$uibModalInstance.close(site);
          })
          .catch(err=> {
            this.isDisabled = false;
            this.errors = {};
            this.stopSpin('spinner-1');
            this.handleError(err, form);
          });
      }
    }

    handleError(err, form) {
      if (err && err.data && err.data.meta && err.data.meta.error_message) {
        if (err.data.meta.code && err.data.meta.code != 422 && !this.Util.isArray(err.data.meta.error_message)) {
          this.serverError = err.data.meta.error_message || 'Internal Server Error';
        } else {
          err.data.meta.error_message.forEach(errorMesssage=> {
            for (let fieldName in errorMesssage) {
              if (form[fieldName]) {
                form[fieldName].$setValidity('mongoose', false);
                this.errors[fieldName] = errorMesssage[fieldName];
              }
            }
          })
        }
      } else {
        this.serverError = 'Internal Server Error';
        console.log(err);
      }
    }

    cancelSave() {
      this.$uibModalInstance.dismiss('cancel');
    }

    startSpin(spinner) {
      this.usSpinnerService.spin(spinner);
    }

    stopSpin(spinner) {
      this.usSpinnerService.stop(spinner);
    }
  }

  angular.module('windmanagerApp')
    .controller('AddEditSiteModalCtrl', AddEditSiteModalCtrl);
})();
