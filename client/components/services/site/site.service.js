'use strict';

(function() {
  function SiteResource($resource) {
    return $resource('api/v1/sites/:id/:controller/:page/:limit/:sortBy/:order', {
      id: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }

  angular.module('windmanagerApp')
    .factory('Site', SiteResource);

})();
