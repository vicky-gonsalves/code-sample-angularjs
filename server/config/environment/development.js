'use strict';

// Development specific configuration
// ==================================
module.exports = {

  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/windmanager-dev'
  },
  // GOOGLE_API_KEY: 'API_KEY HERE',
  // Seed database on startup
  seedDB: true

};
