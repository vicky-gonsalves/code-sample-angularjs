'use strict';

describe('Controller: AddEditBladeCtrl', function () {

  // load the controller's module
  beforeEach(module('windmanagerApp'));

  var AddEditBladeCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AddEditBladeCtrl = $controller('AddEditBladeCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
