'use strict';

describe('Component: TurbineComponent', function () {

  // load the controller's module
  beforeEach(module('windmanagerApp'));

  var TurbineComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    TurbineComponent = $componentController('TurbineComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
