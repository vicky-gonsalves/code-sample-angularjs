'use strict';
(function() {

  class TurbineComponent {
    constructor(Auth, usSpinnerService, Turbine, $stateParams, $state) {
      this.Turbine = Turbine;
      this.$stateParams = $stateParams;
      this.usSpinnerService = usSpinnerService;
      this.isLoggedIn = Auth.isLoggedIn;
      this.turbine = {};
      if (Auth.isLoggedIn()) {
        this.init();
      } else {
        $state.go('login');
      }
    }

    $onInit() {
    }

    init() {
      this.loading = true;
      this.Turbine.get({id: this.$stateParams.id}).$promise
        .then(response=> {
          if (response.data) {
            this.turbine = response.data;
          }
          this.loading = false;
          this.stopSpin('spinner-1');
        })
        .catch(err=> {
          console.log(err);
          this.stopSpin('spinner-1');
          this.loading = false;
          this.turbine = null;
        })
    }

    startSpin(spinner) {
      this.usSpinnerService.spin(spinner);
    }

    stopSpin(spinner) {
      this.usSpinnerService.stop(spinner);
    }

  }

  angular.module('windmanagerApp')
    .component('turbine', {
      templateUrl: 'app/turbine/turbine.html',
      controller: TurbineComponent,
      controllerAs: 'tc'
    });

})();
