'use strict';

angular.module('windmanagerApp')
  .directive('sidebar', function() {
    return {
      templateUrl: 'components/directives/sidebar/sidebar.html',
      restrict: 'EA',
      controller: 'SidebarCtrl',
      controllerAs: 'sc',
      link: function(scope, element, attrs) {
      }
    };
  });
