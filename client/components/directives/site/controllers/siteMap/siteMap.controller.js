'use strict';

(function() {
  class SiteMapCtrl {
    constructor($scope, Auth, Site, $state, usSpinnerService, $timeout, NgMap, appConfig) {
      this.API_KEY = appConfig.GOOGLE_API_KEY;
      this.$scope = $scope;
      this.Site = Site;
      this.allSites = [];
      this.loading = false;
      this.currentPosition = null;
      this.usSpinnerService = usSpinnerService;
      if (Auth.isLoggedIn()) {
        this.init();
      } else {
        $state.go('login');
      }
      this.$scope.$on('siteAdded', (event, site) => {
        // this.allSites.unshift(site);
        this.init();
        NgMap.getMap().then(function(map) {
          $timeout(()=> {
            google.maps.event.trigger(map, 'resize');
          });
        })
      });
    }

    init() {
      this.startSpin('spinner-2');
      this.loading = true;
      this.Site.get().$promise
        .then(response=> {
          if (response.data && response.data.length) {
            this.allSites = response.data;
            if (this.allSites.length > 0) {
              if (this.allSites[0].location) {
                this.currentPosition = this.getAddress(this.allSites[0].location);
              }
            }
          }
          this.loading = false;
          this.stopSpin('spinner-2');
        })
        .catch(err=> {
          console.log(err);
          this.loading = false;
          this.stopSpin('spinner-2');
        })
    }

    getAddress(location) {
      let address = '';
      if (location.addressLine1 && location.addressLine1.length) {
        address += location.addressLine1;
      }
      if (location.addressLine2 && location.addressLine2.length) {
        address += ', ' + location.addressLine2;
      }
      if (location.addressLine3 && location.addressLine3.length) {
        address += ', ' + location.addressLine3;
      }
      if (location.addressLine4 && location.addressLine4.length) {
        address += ', ' + location.addressLine4;
      }
      if (location.addressLine5 && location.addressLine5.length) {
        address += ', ' + location.addressLine5;
      }
      return address;
    }

    getTotalCapacity(turbines) {
      let capacity = 0;
      if (turbines && turbines.length) {
        turbines.forEach((a) => {
          capacity += a.capacity
        });
        return capacity.toFixed(2);
      }
      return 0;
    }

    startSpin(spinner) {
      this.usSpinnerService.spin(spinner);
    }

    stopSpin(spinner) {
      this.usSpinnerService.stop(spinner);
    }
  }

  angular.module('windmanagerApp')
    .controller('SiteMapCtrl', SiteMapCtrl);
})();
