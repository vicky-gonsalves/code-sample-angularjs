'use strict';

import mongoose from 'mongoose';

var AddressSchema = new mongoose.Schema({
  addressLine1: String,
  addressLine2: String,
  addressLine3: String,  //Town/City
  addressLine4: String,  //County
  addressLine5: String,  //Postal Code
  emails: [{
    type: String
  }],
  contacts: [{
    type: String
  }]
});

var SiteSchema = new mongoose.Schema({
  name: String,
  location: AddressSchema,
  turbines: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Turbine'
  }],
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date
  },
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  updatedBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
});

export default mongoose.model('Site', SiteSchema);
