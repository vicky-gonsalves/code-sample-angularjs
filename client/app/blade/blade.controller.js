'use strict';
(function() {

  class BladeComponent {
    constructor(Auth, usSpinnerService, Blade, $stateParams, $state) {
      this.Blade = Blade;
      this.$stateParams = $stateParams;
      this.usSpinnerService = usSpinnerService;
      this.isLoggedIn = Auth.isLoggedIn;
      this.blade = {};
      if (Auth.isLoggedIn()) {
        this.init();
      } else {
        $state.go('login');
      }
    }

    $onInit() {
    }

    init() {
      this.loading = true;
      this.Blade.get({id: this.$stateParams.id}).$promise
        .then(response=> {
          if (response.data) {
            this.blade = response.data;
          }
          this.loading = false;
          this.stopSpin('spinner-1');
        })
        .catch(err=> {
          console.log(err);
          this.stopSpin('spinner-1');
          this.loading = false;
          this.blade = null;
        })
    }

    startSpin(spinner) {
      this.usSpinnerService.spin(spinner);
    }

    stopSpin(spinner) {
      this.usSpinnerService.stop(spinner);
    }
  }

  angular.module('windmanagerApp')
    .component('blade', {
      templateUrl: 'app/blade/blade.html',
      controller: BladeComponent,
      controllerAs: 'bc'
    });

})();
