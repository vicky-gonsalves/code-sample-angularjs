'use strict';

(function() {
  class ViewTurbineCtrl {
    constructor(appConfig) {
      this.API_KEY = appConfig.GOOGLE_API_KEY;
    }

    getAddress(location) {
      let address = '';
      if (location.addressLine1 && location.addressLine1.length) {
        address += location.addressLine1;
      }
      if (location.addressLine2 && location.addressLine2.length) {
        address += ', ' + location.addressLine2;
      }
      if (location.addressLine3 && location.addressLine3.length) {
        address += ', ' + location.addressLine3;
      }
      if (location.addressLine4 && location.addressLine4.length) {
        address += ', ' + location.addressLine4;
      }
      if (location.addressLine5 && location.addressLine5.length) {
        address += ', ' + location.addressLine5;
      }
      return address;
    }
  }

  angular.module('windmanagerApp')
    .controller('ViewTurbineCtrl', ViewTurbineCtrl);
})();
