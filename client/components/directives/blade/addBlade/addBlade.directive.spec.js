'use strict';

describe('Directive: addBlade', function () {

  // load the directive's module and view
  beforeEach(module('windmanagerApp'));
  beforeEach(module('components/directives/blade/addBlade/addBlade.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<add-blade></add-blade>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the addBlade directive');
  }));
});
