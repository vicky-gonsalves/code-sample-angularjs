'use strict';

(function() {
  class ViewBladeCtrl {
    constructor($scope) {
      this.blade = $scope.blade;
    }
  }
  angular.module('windmanagerApp')
    .controller('ViewBladeCtrl', ViewBladeCtrl);
})();

