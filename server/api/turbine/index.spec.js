'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var turbineCtrlStub = {
  index: 'turbineCtrl.index',
  show: 'turbineCtrl.show',
  create: 'turbineCtrl.create',
  update: 'turbineCtrl.update',
  destroy: 'turbineCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var turbineIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './turbine.controller': turbineCtrlStub
});

describe('Turbine API Router:', function() {

  it('should return an express router instance', function() {
    turbineIndex.should.equal(routerStub);
  });

  describe('GET /api/turbines', function() {

    it('should route to turbine.controller.index', function() {
      routerStub.get
        .withArgs('/', 'turbineCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/turbines/:id', function() {

    it('should route to turbine.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'turbineCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/turbines', function() {

    it('should route to turbine.controller.create', function() {
      routerStub.post
        .withArgs('/', 'turbineCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/turbines/:id', function() {

    it('should route to turbine.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'turbineCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/turbines/:id', function() {

    it('should route to turbine.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'turbineCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/turbines/:id', function() {

    it('should route to turbine.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'turbineCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
