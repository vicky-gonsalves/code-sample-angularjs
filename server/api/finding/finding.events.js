/**
 * Finding model events
 */

'use strict';

import {EventEmitter} from 'events';
import Finding from './finding.model';
var FindingEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
FindingEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  Finding.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    FindingEvents.emit(event + ':' + doc._id, doc);
    FindingEvents.emit(event, doc);
  }
}

export default FindingEvents;
