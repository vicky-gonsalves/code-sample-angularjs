'use strict';

describe('Controller: AddEditSiteModalCtrl', function () {

  // load the controller's module
  beforeEach(module('windmanagerApp'));

  var AddEditSiteModalCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AddEditSiteModalCtrl = $controller('AddEditSiteModalCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
