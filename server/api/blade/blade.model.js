'use strict';

import mongoose from 'mongoose';

var BladeSchema = new mongoose.Schema({
  turbine: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Turbine'
  },
  serial: String,
  make: String,
  model: String,
  position: Number,
  material: String,
  diameter: Number, //1.54 m
  surface: String,
  type: String,
  length: Number,
  condition: Number,
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date
  },
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  updatedBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
});

export default mongoose.model('Blade', BladeSchema);
