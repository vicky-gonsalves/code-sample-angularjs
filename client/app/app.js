'use strict';

angular.module('windmanagerApp', [
  'windmanagerApp.auth',
  'windmanagerApp.admin',
  'windmanagerApp.constants',
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'btford.socket-io',
  'ui.router',
  'ui.bootstrap',
  'validation.match',
  'angularSpinner',
  'ngMap',
  'angularMoment'
])
  .config(function($urlRouterProvider, $locationProvider, usSpinnerConfigProvider) {
    $urlRouterProvider
      .otherwise('/');

    $locationProvider.html5Mode(true);
    usSpinnerConfigProvider.setDefaults({color: '#67d3e0'});
  });
