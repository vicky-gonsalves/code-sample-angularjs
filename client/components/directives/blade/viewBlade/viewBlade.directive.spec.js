'use strict';

describe('Directive: viewBlade', function () {

  // load the directive's module and view
  beforeEach(module('windmanagerApp'));
  beforeEach(module('components/directives/viewBlade/viewBlade.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<view-blade></view-blade>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the viewBlade directive');
  }));
});
