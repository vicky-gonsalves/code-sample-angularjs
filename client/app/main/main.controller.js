'use strict';

(function() {

  class MainController {

    constructor(Auth, usSpinnerService, $timeout) {
      this.usSpinnerService = usSpinnerService;
      this.isLoggedIn = Auth.isLoggedIn;
    }

    $onInit() {
    }

    startSpin(spinner) {
      this.usSpinnerService.spin(spinner);
    }

    stopSpin(spinner) {
      this.usSpinnerService.stop(spinner);
    }

  }

  angular.module('windmanagerApp')
    .component('main', {
      templateUrl: 'app/main/main.html',
      controller: MainController
    });

})();
