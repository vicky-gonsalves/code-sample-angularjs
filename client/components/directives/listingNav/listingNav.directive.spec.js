'use strict';

describe('Directive: listingNav', function () {

  // load the directive's module and view
  beforeEach(module('windmanagerApp'));
  beforeEach(module('components/directives/listingNav/listingNav.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<listing-nav></listing-nav>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the listingNav directive');
  }));
});
