'use strict';

describe('Component: BladeComponent', function () {

  // load the controller's module
  beforeEach(module('windmanagerApp'));

  var BladeComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    BladeComponent = $componentController('BladeComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
