'use strict';

describe('Directive: addSite', function () {

  // load the directive's module and view
  beforeEach(module('windmanagerApp'));
  beforeEach(module('components/directives/addSite/addSite.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<add-site></add-site>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the addSite directive');
  }));
});
