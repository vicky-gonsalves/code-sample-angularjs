'use strict';

describe('Controller: ListTurbineCtrl', function () {

  // load the controller's module
  beforeEach(module('windmanagerApp'));

  var ListTurbineCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ListTurbineCtrl = $controller('ListTurbineCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
