'use strict';

angular.module('windmanagerApp.admin', [
  'windmanagerApp.auth',
  'ui.router'
]);
