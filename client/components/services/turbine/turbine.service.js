'use strict';

(function() {
  function TurbineResource($resource) {
    return $resource('api/v1/turbines/:id/:controller/:page/:limit/:sortBy/:order', {
      id: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }

  angular.module('windmanagerApp')
    .factory('Turbine', TurbineResource);

})();
