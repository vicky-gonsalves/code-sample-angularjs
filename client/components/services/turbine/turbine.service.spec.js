'use strict';

describe('Service: turbine', function () {

  // load the service's module
  beforeEach(module('windmanagerApp'));

  // instantiate service
  var turbine;
  beforeEach(inject(function (_turbine_) {
    turbine = _turbine_;
  }));

  it('should do something', function () {
    expect(!!turbine).toBe(true);
  });

});
