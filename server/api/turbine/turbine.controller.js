'use strict';

import _ from 'lodash';
import Turbine from './turbine.model';
import * as SiteCtrl from '../site/site.controller';

const validator = require('validator');
const Promise = require('bluebird');
const ResponseHelpers = require('../../components/responseHelpers/responseHelpers');
Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});

function sanitizeEntities(data, toValidate = []) {
  return new Promise((resolve, reject)=> {
    try {
      let sanitizedData = JSON.parse(JSON.stringify(data));
      if (data.make && toValidate.indexOf('make') > -1) {
        sanitizedData.make = validator.trim(data.make);
      }
      if (data.model && toValidate.indexOf('model') > -1) {
        sanitizedData.model = validator.trim(data.model);
      }
      if (data.serial && toValidate.indexOf('serial') > -1) {
        sanitizedData.serial = validator.trim(data.serial);
      }
      // if (data.capacity && toValidate.indexOf('capacity') > -1) {
      //   sanitizedData.capacity = validator.trim(data.capacity);
      // }
      // if (data.capacityFactor && toValidate.indexOf('capacityFactor') > -1) {
      //   sanitizedData.capacityFactor = validator.trim(data.capacityFactor);
      // }
      // if (data.sitePosition && toValidate.indexOf('sitePosition') > -1) {
      //   sanitizedData.sitePosition = validator.trim(data.sitePosition);
      // }
      // if (data.position && toValidate.indexOf('position') > -1) {
      //   sanitizedData.position = validator.trim(data.position);
      // }
      if (data.type && toValidate.indexOf('type') > -1) {
        sanitizedData.type = validator.trim(data.type);
      }
      // if (data.diameter && toValidate.indexOf('diameter') > -1) {
      //   sanitizedData.diameter = validator.trim(data.diameter);
      // }
      // if (data.powerRegulation && toValidate.indexOf('powerRegulation') > -1) {
      //   sanitizedData.powerRegulation = validator.trim(data.powerRegulation);
      // }
      if (data.manufactured && toValidate.indexOf('manufactured') > -1) {
        sanitizedData.manufactured = validator.trim(data.manufactured);
      }
      if (data.commisioning && toValidate.indexOf('commisioning') > -1) {
        sanitizedData.commisioning = validator.trim(data.commisioning);
      }
      return resolve(sanitizedData);
    } catch (e) {
      console.log('Error in turbine.controller.sanitizeEntities 1');
      return reject(e);
    }
  })
}

function validateEntities(toValidate = []) {
  return function(data) {
    return new Promise((resolve, reject)=> {
      if (data) {

        let errors = {type: 'InvalidParams', code: 422, msg: []};
        //Validation
        if (toValidate.indexOf('make') > -1) {
          if (!(data.make) || validator.isNull(data.make)) {
            errors.msg.push({make: 'Make is required'});
          } else if (data.make && !validator.isLength(data.make, {min: 1, max: 150})) {
            errors.msg.push({make: 'Make must be between 1 to 150 characters long'});
          }
        }

        if (toValidate.indexOf('model') > -1) {
          if (!(data.model) || validator.isNull(data.model)) {
            errors.msg.push({model: 'Model is required'});
          } else if (data.model && !validator.isLength(data.model, {min: 1, max: 150})) {
            errors.msg.push({model: 'Model must be between 1 to 150 characters long'});
          }
        }


        if (toValidate.indexOf('serial') > -1) {
          if (!(data.serial) || validator.isNull(data.serial)) {
            errors.msg.push({serial: 'Serial is required'});
          } else if (data.serial && !validator.isLength(data.serial, {min: 1, max: 150})) {
            errors.msg.push({serial: 'Serial must be between 1 to 150 characters long'});
          }
        }

        if (toValidate.indexOf('capacity') > -1) {
          if (!(data.capacity)) {
            errors.msg.push({capacity: 'Capacity is required'});
          }
        }

        // if (toValidate.indexOf('capacityFactor') > -1) {
        //   if (!(data.capacityFactor) || validator.isNull(data.capacityFactor)) {
        //     errors.msg.push({capacityFactor: 'Capacity Factor is required'});
        //   } else if (data.capacityFactor && !validator.isLength(data.capacityFactor, {min: 1, max: 150})) {
        //     errors.msg.push({capacityFactor: 'Capacity Factor must be between 1 to 150 characters long'});
        //   }
        // }


        if (toValidate.indexOf('sitePosition') > -1) {
          if (!(data.sitePosition)) {
            errors.msg.push({sitePosition: 'Site Position is required'});
          }
        }

        // if (toValidate.indexOf('position') > -1) {
        //   if (!(data.position) || validator.isNull(data.position)) {
        //     errors.msg.push({position: 'Position is required'});
        //   } else if (data.position && !validator.isLength(data.position, {min: 1, max: 6})) {
        //     errors.msg.push({position: 'Position must be between 1 to 6 characters long'});
        //   }
        // }

        if (toValidate.indexOf('type') > -1) {
          if (!(data.type) || validator.isNull(data.type)) {
            errors.msg.push({type: 'Type is required'});
          } else if (data.type && !validator.isLength(data.type, {min: 1, max: 150})) {
            errors.msg.push({type: 'Type must be between 1 to 150 characters long'});
          }
        }

        // if (toValidate.indexOf('diameter') > -1) {
        //   if (!(data.diameter)) {
        //     errors.msg.push({diameter: 'Diameter is required'});
        //   }
        // }

        // if (toValidate.indexOf('powerRegulation') > -1) {
        //   if (!(data.powerRegulation) || validator.isNull(data.powerRegulation)) {
        //     errors.msg.push({powerRegulation: 'Power Regulation is required'});
        //   } else if (data.powerRegulation && !validator.isLength(data.powerRegulation, {min: 1, max: 150})) {
        //     errors.msg.push({powerRegulation: 'Power Regulation must be between 1 to 150 characters long'});
        //   }
        // }

        if (toValidate.indexOf('manufactured') > -1) {
          if (!(data.manufactured) || validator.isNull(data.manufactured)) {
            errors.msg.push({manufactured: 'Manufactured is required'});
          } else if (data.manufactured && !validator.isDate(data.manufactured)) {
            errors.msg.push({manufactured: 'Manufactured must be a valid date'});
          }
        }

        if (toValidate.indexOf('commisioning') > -1) {
          if (!(data.commisioning) || validator.isNull(data.commisioning)) {
            errors.msg.push({commisioning: 'Commisioning date is required'});
          } else if (data.commisioning && !validator.isDate(data.commisioning)) {
            errors.msg.push({commisioning: 'Commisioning date must be a valid date'});
          }
        }

        if (errors.msg.length > 0) {
          console.log('Error in turbine.controller.validateEntities 1');
          return reject(errors);
        } else {
          return resolve(data);
        }
      } else {
        console.log('Error in turbine.controller.validateEntities 2');
        let err = {type: 'EntityNotFound', code: 404, msg: 'Entity not found.'};
        return reject(err);
      }
    });
  }
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function saveBlade(blade) {
  return function(entity) {
    entity.blades.push(blade);
    entity.markModified('blades');
    return entity.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity() {
  return function(entity) {
    return new Promise((resolve, reject)=> {
      if (entity) {
        return entity.remove()
          .then(() => {
            return resolve();
          })
          .catch(err=> {
            console.log('Error in turbine.controller.removeEntity 1');
            return reject(err);
          })
      } else {
        console.log('Error in turbine.controller.removeEntity 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'Turbine not found'});
      }
    });
  };
}

function addTurbineInSite(site) {
  return function(entity) {
    return new Promise((resolve, reject)=> {
      if (entity) {
        return SiteCtrl.addTurbineInSite(site, entity._id)
          .then(()=> {
            return resolve(entity);
          })
          .catch(err=> {
            console.log('Error in turbine.controller.addTurbineInSite 1');
            return reject(err);
          })
      } else {
        console.log('Error in turbine.controller.addTurbineInSite 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'Entity not found.'});
      }
    })
  }
}

function getSite() {
  return function(turbine) {
    return new Promise((resolve, reject)=> {
      if (turbine) {
        return SiteCtrl.getSiteByTurbineId(turbine._id)
          .then(site=> {
            turbine.site = site;
            return resolve(turbine);
          })
          .catch(err=> {
            console.log('Error in turbine.controller.getSite 1');
            return reject(err);
          })
      } else {
        console.log('Error in turbine.controller.getSite 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'Entity not found.'});
      }
    })
  }
}

function sortByPosition() {
  return function(turbine) {
    return new Promise((resolve, reject)=> {
      if (turbine) {
        turbine.blades.sort((a, b) => a.position - b.position);
        return resolve(turbine);
      } else {
        console.log('Error in turbine.controller.sortByPosition 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'Entity not found.'});
      }
    })
  }
}

export function addBladeInTurbine(turbineId, bladeId) {
  return new Promise((resolve, reject)=> {
    if (turbineId && bladeId) {
      return Turbine.findById(turbineId)
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Turbine not found.'}))
        .then(saveBlade(bladeId))
        .then(turbine=> {
          return resolve(turbine);
        })
        .catch(err=> {
          console.log('Error in turbine.controller.addBladeInTurbine 1');
          console.log(err);
          return reject(err);
        });
    } else {
      console.log('Error in turbine.controller.addBladeInTurbine 2');
      return reject({type: 'MissingParams', code: 404, msg: 'turbineId/bladeId Missing'});
    }
  });
}

export function getTurbineById(turbineId) {
  return new Promise((resolve, reject)=> {
    if (turbineId) {
      return Turbine.findById(turbineId)
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Turbine not found.'}))
        .then(turbine=> {
          return resolve(turbine);
        })
        .catch(err=> {
          console.log('Error in turbine.controller.getTurbineById 1');
          console.log(err);
          return reject(err);
        });
    } else {
      console.log('Error in turbine.controller.getTurbineById 2');
      return reject({type: 'MissingParams', code: 404, msg: 'turbineId Missing'});
    }
  });
}

/*Default Root functions*/
// Gets a list of Turbines
export function index(req, res) {
  return Turbine.find().exec()
    .populate('blades')
    .sort({createdAt: -1})
    .lean()
    .exec()
    .then(sortByPosition())
    .then(ResponseHelpers.respondWithResult(res))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Gets a single Turbine from the DB
export function show(req, res) {
  return Turbine.findById(req.params.id)
    .populate('blades')
    .lean()
    .exec()
    .then(sortByPosition())
    .then(getSite())
    .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Turbine not found.'}))
    .then(ResponseHelpers.respondWithResult(res))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Creates a new Turbine in the DB
export function create(req, res) {
  if (req.body.createdAt) {
    delete req.body.createdAt;
  }
  if (req.body.updatedAt) {
    delete req.body.updatedAt;
  }
  if (req.body.createdBy) {
    delete req.body.createdBy;
  }
  if (req.body.updatedBy) {
    delete req.body.updatedBy;
  }
  if (req.body.blades) {
    delete req.body.blades;
  }
  let toValidate = ['make', 'model', 'serial', 'capacity', 'sitePosition', 'type', 'manufactured', 'commisioning'];
  return sanitizeEntities(req.body, toValidate)
    .then(validateEntities(toValidate))
    .then(data=> {
      data.createdBy = req.user._id;
      return Turbine.create(data)
        .then(addTurbineInSite(data.site))
        .then(ResponseHelpers.respondWithResult(res, 201))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Updates an existing Turbine in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  if (req.body.createdAt) {
    delete req.body.createdAt;
  }
  if (req.body.updatedAt) {
    delete req.body.updatedAt;
  }
  if (req.body.createdBy) {
    delete req.body.createdBy;
  }
  if (req.body.updatedBy) {
    delete req.body.updatedBy;
  }
  if (req.body.blades) {
    delete req.body.blades;
  }
  let toValidate = ['make', 'model', 'serial', 'capacity', 'sitePosition', 'type', 'manufactured', 'commisioning'];
  return sanitizeEntities(req.body, toValidate)
    .then(validateEntities(toValidate))
    .then(data=> {
      data.updatedBy = req.user._id;
      data.updatedAt = Date.now();
      return Turbine.findById(req.params.id).exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Turbine not found.'}))
        .then(saveUpdates(data))
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Deletes a Turbine from the DB
export function destroy(req, res) {
  return Turbine.findById(req.params.id).exec()
    .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Turbine not found.'}))
    .then(removeEntity())
    .then(ResponseHelpers.respondWithResult(res, 204))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}
