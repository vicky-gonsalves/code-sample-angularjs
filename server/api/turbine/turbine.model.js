'use strict';

import mongoose from 'mongoose';

var TurbineSchema = new mongoose.Schema({
  make: String, //EU
  model: String, //826 015 089
  serial: String, //1234123412
  capacity: Number, //2.3MW
  sitePosition: Number,
  type: String, //3-bladed, horizontal axis
  manufactured: Date,
  commisioning: Date,
  blades: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Blade'
  }],
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date
  },
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  updatedBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
});

export default mongoose.model('Turbine', TurbineSchema);
