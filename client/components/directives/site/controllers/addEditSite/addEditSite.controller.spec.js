'use strict';

describe('Controller: AddEditSiteCtrl', function () {

  // load the controller's module
  beforeEach(module('windmanagerApp'));

  var AddEditSiteCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AddEditSiteCtrl = $controller('AddEditSiteCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
