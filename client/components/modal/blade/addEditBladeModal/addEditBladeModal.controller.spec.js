'use strict';

describe('Controller: AddEditBladeModalCtrl', function () {

  // load the controller's module
  beforeEach(module('windmanagerApp'));

  var AddEditBladeModalCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AddEditBladeModalCtrl = $controller('AddEditBladeModalCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
