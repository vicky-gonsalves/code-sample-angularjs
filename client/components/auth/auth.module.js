'use strict';

angular.module('windmanagerApp.auth', [
  'windmanagerApp.constants',
  'windmanagerApp.util',
  'ngCookies',
  'ui.router'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
