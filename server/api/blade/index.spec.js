'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var bladeCtrlStub = {
  index: 'bladeCtrl.index',
  show: 'bladeCtrl.show',
  create: 'bladeCtrl.create',
  update: 'bladeCtrl.update',
  destroy: 'bladeCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var bladeIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './blade.controller': bladeCtrlStub
});

describe('Blade API Router:', function() {

  it('should return an express router instance', function() {
    bladeIndex.should.equal(routerStub);
  });

  describe('GET /api/blades', function() {

    it('should route to blade.controller.index', function() {
      routerStub.get
        .withArgs('/', 'bladeCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/blades/:id', function() {

    it('should route to blade.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'bladeCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/blades', function() {

    it('should route to blade.controller.create', function() {
      routerStub.post
        .withArgs('/', 'bladeCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/blades/:id', function() {

    it('should route to blade.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'bladeCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/blades/:id', function() {

    it('should route to blade.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'bladeCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/blades/:id', function() {

    it('should route to blade.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'bladeCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
