'use strict';

describe('Directive: listTurbine', function () {

  // load the directive's module and view
  beforeEach(module('windmanagerApp'));
  beforeEach(module('components/directives/turbine/listTurbine/listTurbine.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<list-turbine></list-turbine>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the listTurbine directive');
  }));
});
