'use strict';

angular.module('windmanagerApp')
  .directive('customHover', function() {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        $(element).touchHover();
      }
    };
  });
