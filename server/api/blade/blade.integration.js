'use strict';

var app = require('../..');
import request from 'supertest';

var newBlade;

describe('Blade API:', function() {

  describe('GET /api/blades', function() {
    var blades;

    beforeEach(function(done) {
      request(app)
        .get('/api/blades')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          blades = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      blades.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/blades', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/blades')
        .send({
          name: 'New Blade',
          info: 'This is the brand new blade!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newBlade = res.body;
          done();
        });
    });

    it('should respond with the newly created blade', function() {
      newBlade.name.should.equal('New Blade');
      newBlade.info.should.equal('This is the brand new blade!!!');
    });

  });

  describe('GET /api/blades/:id', function() {
    var blade;

    beforeEach(function(done) {
      request(app)
        .get('/api/blades/' + newBlade._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          blade = res.body;
          done();
        });
    });

    afterEach(function() {
      blade = {};
    });

    it('should respond with the requested blade', function() {
      blade.name.should.equal('New Blade');
      blade.info.should.equal('This is the brand new blade!!!');
    });

  });

  describe('PUT /api/blades/:id', function() {
    var updatedBlade;

    beforeEach(function(done) {
      request(app)
        .put('/api/blades/' + newBlade._id)
        .send({
          name: 'Updated Blade',
          info: 'This is the updated blade!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedBlade = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedBlade = {};
    });

    it('should respond with the updated blade', function() {
      updatedBlade.name.should.equal('Updated Blade');
      updatedBlade.info.should.equal('This is the updated blade!!!');
    });

  });

  describe('DELETE /api/blades/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/blades/' + newBlade._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when blade does not exist', function(done) {
      request(app)
        .delete('/api/blades/' + newBlade._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
