'use strict';

import _ from 'lodash';
import Site from './site.model';
const validator = require('validator');
const Promise = require('bluebird');
const ResponseHelpers = require('../../components/responseHelpers/responseHelpers');
Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});

function sanitizeEntities(data, toValidate = []) {
  return new Promise((resolve, reject)=> {
    try {
      let sanitizedData = JSON.parse(JSON.stringify(data));
      if (data.name && toValidate.indexOf('name') > -1) {
        sanitizedData.name = validator.trim(data.name);
      }
      if (data.location.addressLine1 && toValidate.indexOf('addressLine1') > -1) {
        sanitizedData.location.addressLine1 = validator.trim(data.location.addressLine1);
      }
      if (data.location.addressLine2 && toValidate.indexOf('addressLine2') > -1) {
        sanitizedData.location.addressLine2 = validator.trim(data.location.addressLine2);
      }
      if (data.location.addressLine3 && toValidate.indexOf('addressLine3') > -1) {
        sanitizedData.location.addressLine3 = validator.trim(data.location.addressLine3);
      }
      if (data.location.addressLine4 && toValidate.indexOf('addressLine4') > -1) {
        sanitizedData.location.addressLine4 = validator.trim(data.location.addressLine4);
      }
      if (data.location.addressLine5 && toValidate.indexOf('addressLine5') > -1) {
        sanitizedData.location.addressLine5 = validator.trim(data.location.addressLine5);
      }
      return resolve(sanitizedData);
    } catch (e) {
      console.log('Error in site.controller.sanitizeEntities 1');
      return reject(e);
    }
  })
}

function validateEntities(toValidate = []) {
  return function(data) {
    return new Promise((resolve, reject)=> {
      if (data) {
        let errors = {type: 'InvalidParams', code: 422, msg: []};
        //Validation
        if (toValidate.indexOf('name') > -1) {
          if (!(data.name) || validator.isNull(data.name)) {
            errors.msg.push({name: 'name is required'});
          } else if (data.name && !validator.isLength(data.name, {min: 1, max: 150})) {
            errors.msg.push({name: 'name must be between 1 to 150 characters long'});
          }
        }

        if (toValidate.indexOf('addressLine1') > -1) {
          if (!(data.location.addressLine1) || validator.isNull(data.location.addressLine1)) {
            errors.msg.push({addressLine1: 'addressLine1 is required'});
          } else if (data.location.addressLine1 && !validator.isLength(data.location.addressLine1, {min: 1, max: 150})) {
            errors.msg.push({addressLine1: 'addressLine1 must be between 1 to 150 characters long'});
          }
        }


        if (toValidate.indexOf('addressLine2') > -1) {
          if (!(data.location.addressLine2) || validator.isNull(data.location.addressLine2)) {
            errors.msg.push({addressLine2: 'addressLine2 is required'});
          } else if (data.location.addressLine2 && !validator.isLength(data.location.addressLine2, {min: 1, max: 150})) {
            errors.msg.push({addressLine2: 'addressLine2 must be between 1 to 150 characters long'});
          }
        }


        if (toValidate.indexOf('addressLine3') > -1) {
          if (!(data.location.addressLine3) || validator.isNull(data.location.addressLine3)) {
            errors.msg.push({addressLine3: 'Town/City is required'});
          } else if (data.location.addressLine3 && !validator.isLength(data.location.addressLine3, {min: 1, max: 150})) {
            errors.msg.push({addressLine3: 'Town/City must be between 1 to 150 characters long'});
          }
        }


        if (toValidate.indexOf('addressLine4') > -1) {
          if (!(data.location.addressLine4) || validator.isNull(data.location.addressLine4)) {
            errors.msg.push({addressLine4: 'County/State is required'});
          } else if (data.location.addressLine4 && !validator.isLength(data.location.addressLine4, {min: 1, max: 150})) {
            errors.msg.push({addressLine4: 'County/State must be between 1 to 150 characters long'});
          }
        }


        if (toValidate.indexOf('addressLine5') > -1) {
          if (!(data.location.addressLine5) || validator.isNull(data.location.addressLine5)) {
            errors.msg.push({addressLine5: 'Postal/Zip Code is required'});
          } else if (data.location.addressLine5 && !validator.isLength(data.location.addressLine5, {min: 1, max: 6})) {
            errors.msg.push({addressLine5: 'Postal/Zip Code must be between 1 to 6 characters long'});
          }
        }

        if (errors.msg.length > 0) {
          console.log('Error in site.controller.validateEntities 1');
          return reject(errors);
        } else {
          return resolve(data);
        }
      } else {
        console.log('Error in site.controller.validateEntities 2');
        let err = {type: 'EntityNotFound', code: 404, msg: 'Entity not found.'};
        return reject(err);
      }
    });
  }
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function saveTurbine(turbine) {
  return function(entity) {
    entity.turbines.push(turbine);
    entity.markModified('turbines');
    return entity.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity() {
  return function(entity) {
    return new Promise((resolve, reject)=> {
      if (entity) {
        return entity.remove()
          .then(() => {
            return resolve();
          })
          .catch(err=> {
            console.log('Error in site.controller.removeEntity 1');
            return reject(err);
          })
      } else {
        console.log('Error in site.controller.removeEntity 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'Site not found'});
      }
    });
  };
}

export function addTurbineInSite(siteId, turbineId) {
  return new Promise((resolve, reject)=> {
    if (siteId && turbineId) {
      return Site.findById(siteId)
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Site not found.'}))
        .then(saveTurbine(turbineId))
        .then(site=> {
          return resolve(site);
        })
        .catch(err=> {
          console.log('Error in site.controller.addTurbineInSite 1');
          console.log(err);
          return reject(err);
        });

    } else {
      console.log('Error in site.controller.addTurbineInSite 2');
      return reject({type: 'MissingParams', code: 404, msg: 'siteId/turbineId Missing'});
    }
  });
}

export function getSiteByTurbineId(turbineId) {
  return new Promise((resolve, reject)=> {
    if (turbineId) {
      return Site.findOne({'turbines': turbineId})
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Site not found.'}))
        .then(site=> {
          return resolve(site);
        })
        .catch(err=> {
          console.log('Error in site.controller.getSiteByTurbineId 1');
          console.log(err);
          return reject(err);
        });
    } else {
      console.log('Error in site.controller.getSiteByTurbineId 2');
      return reject({type: 'MissingParams', code: 422, msg: 'turbineId Missing'});
    }
  });
}

/*Default Root functions*/
// Gets a list of Sites
export function index(req, res) {
  return Site.find()
    .populate('turbines')
    .sort({createdAt: -1})
    .exec()
    .then(ResponseHelpers.respondWithResult(res))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Gets a single Site from the DB
export function show(req, res) {
  return Site.findById(req.params.id).exec()
    .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Site not found.'}))
    .then(ResponseHelpers.respondWithResult(res))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Creates a new Site in the DB
export function create(req, res) {
  if (req.body.createdAt) {
    delete req.body.createdAt;
  }
  if (req.body.updatedAt) {
    delete req.body.updatedAt;
  }
  if (req.body.createdBy) {
    delete req.body.createdBy;
  }
  if (req.body.updatedBy) {
    delete req.body.updatedBy;
  }
  let toValidate = ['name', 'addressLine1', 'addressLine3', 'addressLine4', 'addressLine5'];
  return sanitizeEntities(req.body, toValidate)
    .then(validateEntities(toValidate))
    .then(data=> {
      data.createdBy = req.user._id;
      return Site.create(data)
        .then(ResponseHelpers.respondWithResult(res, 201))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Updates an existing Site in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  if (req.body.createdAt) {
    delete req.body.createdAt;
  }
  if (req.body.updatedAt) {
    delete req.body.updatedAt;
  }
  if (req.body.createdBy) {
    delete req.body.createdBy;
  }
  if (req.body.updatedBy) {
    delete req.body.updatedBy;
  }
  let toValidate = ['name', 'addressLine1', 'addressLine3', 'addressLine4', 'addressLine5'];
  return sanitizeEntities(req.body, toValidate)
    .then(validateEntities(toValidate))
    .then(data=> {
      data.updatedBy = req.user._id;
      data.updatedAt = Date.now();
      return Site.findById(req.params.id).exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Site not found.'}))
        .then(saveUpdates(data))
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Deletes a Site from the DB
export function destroy(req, res) {
  return Site.findById(req.params.id).exec()
    .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Site not found.'}))
    .then(removeEntity())
    .then(ResponseHelpers.respondWithResult(res, 204))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}
