'use strict';

(function() {
  class ListBladeCtrl {
    constructor($scope) {
      this.blades = $scope.blades;
    }
  }

  angular.module('windmanagerApp')
    .controller('ListBladeCtrl', ListBladeCtrl);
})();
