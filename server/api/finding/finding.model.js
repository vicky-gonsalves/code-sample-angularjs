'use strict';

import mongoose from 'mongoose';

var FindingSchema = new mongoose.Schema({
  blade: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Blade'
  },
  category: Number,
  type: String,
  date: Date,
  status: String,
  side: String,
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date
  },
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  updatedBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
});

export default mongoose.model('Finding', FindingSchema);
