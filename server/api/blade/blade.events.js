/**
 * Blade model events
 */

'use strict';

import {EventEmitter} from 'events';
import Blade from './blade.model';
var BladeEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
BladeEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  Blade.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    BladeEvents.emit(event + ':' + doc._id, doc);
    BladeEvents.emit(event, doc);
  }
}

export default BladeEvents;
