'use strict';

describe('Directive: addTurbine', function () {

  // load the directive's module and view
  beforeEach(module('windmanagerApp'));
  beforeEach(module('components/directives/addTurbine/addTurbine.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<add-turbine></add-turbine>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the addTurbine directive');
  }));
});
